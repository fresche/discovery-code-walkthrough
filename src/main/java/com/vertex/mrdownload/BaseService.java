package com.vertex.mrdownload;

import org.springframework.beans.factory.annotation.Autowired;

public abstract class BaseService {

    @Autowired
    private ProgramStatusDataStructure programStatusDataStructure;

    public BaseService(String programName) {
        programStatusDataStructure.setProgramName(programName);
    }
}
