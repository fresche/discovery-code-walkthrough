package com.vertex.mrdownload.global.sqlerrorhandlingmethods;

import com.vertex.mrdownload.BaseService;

public abstract class BaseSqlErrorHandlingMethodsService extends BaseService {

    public BaseSqlErrorHandlingMethodsService() {
        super("GLBSQLERR");
    }
}
