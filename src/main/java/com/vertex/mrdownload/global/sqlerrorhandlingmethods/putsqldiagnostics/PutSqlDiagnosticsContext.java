package com.vertex.mrdownload.global.sqlerrorhandlingmethods.putsqldiagnostics;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PutSqlDiagnosticsContext {

    // Parameters
    private String inProgramName;
    private String inSubprocedureName;
    private String inHostVariables;
    private String inSqlCaPointer;
}
