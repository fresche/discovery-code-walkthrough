package com.vertex.mrdownload.global.enterpriseleveltypedefinitions;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class BatchId {

    public static final BatchId BLANK = new BatchId("");

    private final String value;
}
