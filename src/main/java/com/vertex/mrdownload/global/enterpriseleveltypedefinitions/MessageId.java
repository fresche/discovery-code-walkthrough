package com.vertex.mrdownload.global.enterpriseleveltypedefinitions;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class MessageId {

    public static final MessageId ERROR_UNKNOWN_INTERFACE_ERROR = new MessageId("GBL0001");
    public static final MessageId ERROR_DATABASE_ACCESS_ERROR = new MessageId("GBL0002");
    public static final MessageId ERROR_INVALID_LANGUAGE_CODE = new MessageId("GBL0004");
    public static final MessageId ERROR_ROW_UPDATED_SINCE_LAST_RETRIEVAL = new MessageId("GBL0017");
    public static final MessageId ERROR_ROW_DELETED_SINCE_LAST_RETRIEVAL = new MessageId("GBL0018");
    public static final MessageId ERROR_INVALID_OUTPUT_QUEUE = new MessageId("GBL0038");
    public static final MessageId ERROR_DATA_LOCKED_BY_ANOTHER_USER = new MessageId("GBL0070");
    public static final MessageId ERROR_INVALID_START_OR_EFFECTIVE_DATE = new MessageId("GBL0082");
    public static final MessageId ERROR_END_DATE_PRIOR_TO_START_DATE = new MessageId("GBL0083");
    public static final MessageId ERROR_INVALID_TABLE_NAME = new MessageId("GBL0085");
    public static final MessageId ERROR_INVALID_DATABASE_SCHEMA = new MessageId("GBL0088");
    public static final MessageId ERROR_SELECTION_CRITERIA_REQUIRED = new MessageId("GBL0089");
    public static final MessageId ERROR_INVALID_COLUMN_NAME = new MessageId("GBL0091");
    public static final MessageId ERROR_POINTER_NOT_PROVIDED = new MessageId("GBL0092");
    public static final MessageId ERROR_INVALID_PROGRAM_NAME = new MessageId("GBL0097");
    public static final MessageId WARNING_ALL_MATCHING_ROWS_HAVE_BEEN_RETURNED = new MessageId("GBL0100");
    public static final MessageId ERROR_REFERENTIAL_INTEGRITY_PREVENTED_DELETE = new MessageId("GBL0103");
    public static final MessageId ERROR_REFERENTIAL_INTEGRITY_PREVENTED_INSERT = new MessageId("GBL0122");
    public static final MessageId ERROR_REFERENTIAL_INTEGRITY_PREVENTED_UPDATE = new MessageId("GBL0123");
    public static final MessageId ERROR_INVALID_NUMBER_DECIMAL_POSITIONS = new MessageId("GBL0124");
    public static final MessageId ERROR_MULTIPLE_ROWS_RETURNED = new MessageId("GBL0133");

    public static final MessageId ERROR_INVALID_PROCESS_TYPE = new MessageId("APS0216");
    public static final MessageId ERROR_INVALID_PROCESS_INITIATION_METHOD = new MessageId("APS0225");
    public static final MessageId ERROR_INVALID_PROCESS_REQUEST_TIMESTAMP = new MessageId("APS0226");
    public static final MessageId ERROR_INVALID_PROCESS_ID = new MessageId("APS0272");
    public static final MessageId ERROR_INVALID_PROCESS_ENTRY_REPORT_ID = new MessageId("APS0275");
    public static final MessageId ERROR_A_POSTING_PROCESS_IS_ACTIVE = new MessageId("APS0439");
    public static final MessageId ERROR_INVALID_PROCESS_REQUEST_ID = new MessageId("APS0217");

    public static final MessageId ERROR_INVALID_TASK_ID = new MessageId("APS0193");
    public static final MessageId ERROR_INVALID_TASK_TYPE = new MessageId("APS0212");
    public static final MessageId ERROR_NO_TASK_TYPE_ASSOCIATED_WITH_PROCESS_TYPE = new MessageId("APS0227");

    public static final MessageId METER_READ_DOWNLOAD_TERMINATING_WITH_ERROR = new MessageId("MTR0056");

    public static final MessageId BLANK = new MessageId("");

    private final String value;
}
