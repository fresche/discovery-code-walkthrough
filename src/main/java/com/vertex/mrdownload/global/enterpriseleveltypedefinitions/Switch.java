package com.vertex.mrdownload.global.enterpriseleveltypedefinitions;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Switch {

    private final String value;
}
