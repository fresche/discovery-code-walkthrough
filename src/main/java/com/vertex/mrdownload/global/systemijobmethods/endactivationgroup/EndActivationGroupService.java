package com.vertex.mrdownload.global.systemijobmethods.endactivationgroup;

import com.vertex.mrdownload.global.systemijobmethods.BaseSystemIJobMethodsService;
import org.springframework.stereotype.Service;

/**
 * =======================================================================
 * @subprocedure endActivationGroup
 * @module       GBLJOB02
 * =======================================================================
 * @purpose    This procedure will immediately end the current
 *             Activation Group.
 *             <P>
 *             Any uncommitted database changes will be rolled back.
 *             <P>
 *             <B>NOTE:</B> This procedure is intended for use with
 *             severe errors when it is necessary to immediately and
 *             completely terminate the task.
 *             <P>
 *             Remember that since most E-CIS programs run within
 *             Activation Group *CALLER, all modules in the stack for the
 *             current Activation Group will be IMMEDIATELY terminated.
 *             As such, this procedure should be used with <B>EXTREME
 *             CAUTION</B>.
 *             <P>
 *             Additionally, all collection of diagnostic information and
 *             user notification needs to occur prior to invocation of
 *             this procedure.
 * =======================================================================
 * - Constraints / Assertions -
 * ============================
 * @assertion  This procedure invokes the <I>exit()</I> procedure
 *             which is part of the IBM-supplied QC2LE Binding Directory.
 *             QC2LE contains general-purpose C-language routines.
 * @assertion  This procedure provides indirect access to the exit()
 *             procedure, thus avoiding the need for referencing
 *             routines to directly bind in QC2LE.
 * @assertion  Uncommitted database changes will be rolled back (ROLLBACK)
 *             prior to ending the activation group.  If for some reason,
 *             rollback is not desired, it is the responsibility of the
 *             calling routine to issue a COMMIT (procedure
 *             commitChanges_GBLCOMMIT) prior to calling this procedure.
 * =======================================================================
 * - Parameters -
 * ==============
 * @return     n/a                   Nothing is left to return a value to.
 * =======================================================================
 * - Possible Completion Message Ids -
 * ===================================
 * @throws     n/a      Nothing is left to throw an exception to.
 * =======================================================================
 */
@Service
public class EndActivationGroupService extends BaseSystemIJobMethodsService {

    public String execute(EndActivationGroupContext context) {
        // TODO: Implementation is missing

        return "";
    }
}
