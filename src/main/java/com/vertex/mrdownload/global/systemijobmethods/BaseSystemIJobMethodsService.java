package com.vertex.mrdownload.global.systemijobmethods;

import com.vertex.mrdownload.BaseService;

public abstract class BaseSystemIJobMethodsService extends BaseService {

    public BaseSystemIJobMethodsService() {
        super("GLBJOB");
    }
}
