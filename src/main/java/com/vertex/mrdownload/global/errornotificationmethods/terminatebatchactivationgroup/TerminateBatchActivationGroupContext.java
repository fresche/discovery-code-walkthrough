package com.vertex.mrdownload.global.errornotificationmethods.terminatebatchactivationgroup;

import com.vertex.mrdownload.caps.batchthreadmethods.ThreadJob;
import com.vertex.mrdownload.caps.packageleveltypedefinitions.ProcessId;
import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.BatchId;
import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.Switch;
import com.vertex.mrdownload.global.errornotificationmethods.ErrorNotification;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class TerminateBatchActivationGroupContext {

    // Parameters
    private String inProgramId;
    private String inSubprocedureName;
    private BatchId inBatchId;
    private MessageId inOverrideEmailMessageId;
    private MessageId inErrorMessageId;
    private String inMessageText;

    private int sqlCode;

    // Local Variables
    private String dynamicSqlString;
    private ErrorNotification dsErrorNotification;
    private ThreadJob dsThreadJob;
    private String sndQName;
    private String sndQLibrary;
    private int sndDataLen;
    private String sndData;
    private int sndKeyLen;
    private String sndKeyData;
    private String dataQData;
    private String dqTask;
    private String dqJob;
    private String dqStatus;
    private String command;
    private LocalDateTime createTimestamp;
    private MessageId emailMessageId;
    private String emailMessageText;
    private String errorNotificationProgram;
    private String errorNotificationDistributionGroup;
    private Boolean icom400Available;
    private MessageId messageId;
    private MessageId messageIdOut;
    private ProcessId processId;
    private String processType;
    private Switch reply;
}
