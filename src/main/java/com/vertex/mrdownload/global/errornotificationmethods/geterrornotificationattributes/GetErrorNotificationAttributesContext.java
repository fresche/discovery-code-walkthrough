package com.vertex.mrdownload.global.errornotificationmethods.geterrornotificationattributes;

import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class GetErrorNotificationAttributesContext {

    // Parameters
    private MessageId outMessageId;
}
