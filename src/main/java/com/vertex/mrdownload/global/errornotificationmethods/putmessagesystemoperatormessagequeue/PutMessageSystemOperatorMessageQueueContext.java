package com.vertex.mrdownload.global.errornotificationmethods.putmessagesystemoperatormessagequeue;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PutMessageSystemOperatorMessageQueueContext {

    // Parameters
    private String inProgramId;
    private String inSubprocedureName;
    private String inMessageId;
    private String inMessageData;
    private String outMessageId;
}