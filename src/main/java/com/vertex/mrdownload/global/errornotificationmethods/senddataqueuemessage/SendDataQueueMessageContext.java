package com.vertex.mrdownload.global.errornotificationmethods.senddataqueuemessage;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SendDataQueueMessageContext {

    // Parameters
    private String dataQueueName;
    private String dataQueueLibrary;
    private int dataQueueLength;
    private String data;
    private int keyLength;
    private String keyData;
}