package com.vertex.mrdownload.global.errornotificationmethods.putjobinformationmessage;

import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PutJobInformationMessageContext {

    // Parameters
    private MessageId outMessageId;
}
