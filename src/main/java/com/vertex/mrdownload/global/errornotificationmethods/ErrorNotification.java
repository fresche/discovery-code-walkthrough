package com.vertex.mrdownload.global.errornotificationmethods;

import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ErrorNotification {

    private Boolean enICOM400Available;
    private String enEMailNotificationProgram;
    private MessageId enDefaultEMailMessageId;
    private String enNotificationEMailAddressId;
    private String enNotificationEMailAddress;
}