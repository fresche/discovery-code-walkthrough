package com.vertex.mrdownload.global.errornotificationmethods.sendemailmessageicm;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SendEmailMessageIcmContext {

    // Parameters
    private String distributionName;
    private String messageText;
}
