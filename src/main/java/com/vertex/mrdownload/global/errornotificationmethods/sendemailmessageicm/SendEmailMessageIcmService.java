package com.vertex.mrdownload.global.errornotificationmethods.sendemailmessageicm;

import com.vertex.mrdownload.global.errornotificationmethods.BaseErrorNotificationMethodsService;
import com.vertex.mrdownload.global.errornotificationmethods.ErrorNotification;
import org.springframework.stereotype.Service;

/**
 * =======================================================================
 * @subprocedure sendEMailMessageICM                                      
 * @module       GBLSNDMAIL                                               
 * =======================================================================
 * @purpose    This external program will send e-mail via ICOM400.        
 * =======================================================================
 * - Parameters -                                                         
 * ==============                                                         
 * @return     n/a                   External programs cannot return an   
 *                                   argument.                            
 * @param      distributionName      This is the name of the ICOM/400     
 *                                   Distribution Group to which the      
 *                                   e-mail is to be sent.                
 * @param      messageText           This is the text of the e-mail to be 
 *                                   sent.                                
 * =======================================================================
 */
@Service
public class SendEmailMessageIcmService extends BaseErrorNotificationMethodsService {

    public void execute(SendEmailMessageIcmContext context) {
        // TODO: Implementation is missing
    }
}
