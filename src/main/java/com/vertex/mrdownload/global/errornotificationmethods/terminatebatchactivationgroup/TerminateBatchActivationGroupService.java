package com.vertex.mrdownload.global.errornotificationmethods.terminatebatchactivationgroup;

import com.vertex.mrdownload.ProgramStatusDataStructure;
import com.vertex.mrdownload.caps.batchthreadmethods.getthreadjobdatatableattributes.GetThreadJobDataTableAttributesContext;
import com.vertex.mrdownload.caps.batchthreadmethods.getthreadjobdatatableattributes.GetThreadJobDataTableAttributesService;
import com.vertex.mrdownload.caps.packageleveltypedefinitions.ProcessId;
import com.vertex.mrdownload.caps.processentrymethods.failprocessentry.FailProcessEntryContext;
import com.vertex.mrdownload.caps.processentrymethods.failprocessentry.FailProcessEntryService;
import com.vertex.mrdownload.caps.processentrymethods.iscurrentprocesscritical.IsCurrentProcessEntryCriticalContext;
import com.vertex.mrdownload.caps.processentrymethods.iscurrentprocesscritical.IsCurrentProcessEntryCriticalService;
import com.vertex.mrdownload.caps.processentrymethods.iscurrentprocessstoredprocedure.IsCurrentProcessStoredProcedureContext;
import com.vertex.mrdownload.caps.processentrymethods.iscurrentprocessstoredprocedure.IsCurrentProcessStoredProcedureService;
import com.vertex.mrdownload.caps.processentrymethods.setcurrentprocesscompletionmessageid.SetCurrentProcessCompletionMessageIdContext;
import com.vertex.mrdownload.caps.processentrymethods.setcurrentprocesscompletionmessageid.SetCurrentProcessCompletionMessageIdService;
import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import com.vertex.mrdownload.global.errornotificationmethods.BaseErrorNotificationMethodsService;
import com.vertex.mrdownload.global.errornotificationmethods.geterrornotificationattributes.GetErrorNotificationAttributesContext;
import com.vertex.mrdownload.global.errornotificationmethods.putjobinformationmessage.PutJobInformationMessageContext;
import com.vertex.mrdownload.global.errornotificationmethods.putjobinformationmessage.PutJobInformationMessageService;
import com.vertex.mrdownload.global.errornotificationmethods.putmessagesystemoperatormessagequeue.PutMessageSystemOperatorMessageQueueContext;
import com.vertex.mrdownload.global.errornotificationmethods.putmessagesystemoperatormessagequeue.PutMessageSystemOperatorMessageQueueService;
import com.vertex.mrdownload.global.errornotificationmethods.senddataqueuemessage.SendDataQueueMessageContext;
import com.vertex.mrdownload.global.errornotificationmethods.senddataqueuemessage.SendDataQueueMessageService;
import com.vertex.mrdownload.global.errornotificationmethods.sendemailmessagecustom.SendEmailMessageCustomContext;
import com.vertex.mrdownload.global.errornotificationmethods.sendemailmessageicm.SendEmailMessageIcmContext;
import com.vertex.mrdownload.global.messagehandlingmethods.getexpandedmessagetextsecondary.GetExpandedMessageTextSecondaryContext;
import com.vertex.mrdownload.global.programerrorhandlingmethods.putprogramdiagnostics.PutProgramDiagnosticsContext;
import com.vertex.mrdownload.global.sqlerrorhandlingmethods.putsqldiagnostics.PutSqlDiagnosticsContext;
import com.vertex.mrdownload.global.systemijobmethods.endactivationgroup.EndActivationGroupContext;
import com.vertex.mrdownload.global.systemijobmethods.endactivationgroup.EndActivationGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.util.Optional;

import static com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId.BLANK;
import static com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId.ERROR_UNKNOWN_INTERFACE_ERROR;
import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.trim;

/**
 * =======================================================================
 * @subprocedure terminateBatchActivationGroup                            
 * @module       GBLERRNTF2                                               
 * =======================================================================
 * @purpose    In the event of an error having a process scope (as opposed
 *             to a transactional scope), this procedure will:            
 *         <P>                                                            
 *         <P> 1) Log the error to UPGMDIAG.                              
 *         <P> 2) Write the contents of the job log to UJOBLOG.           
 *         <P> 3) Send a message to the user group via ICOM/400.          
 *         <P> 4) Send a message to the System Operator Message Queue.    
 *         <P> 5) End the Activation Group                                
 * =======================================================================
 * - Constraints / Assertions -                                           
 * ============================                                           
 * @assertion  No return from this procedure is possible.                 
 * @assertion  This method uses a method from the CAPS Service Program,   
 *             APSPRECENT1, to update the current Process Entry with a    
 *             "FAILED" status.                                           
 * @assertion  If the process is not a "critical" process (per Process    
 *             Type configuration), the step for sending a message to the 
 *             System Operator Message Queue (QSYSOPR) will be skipped.   
 * =======================================================================
 * - Parameters -                                                         
 * ==============                                                         
 * @return     n/a                   No return from this procedure is     
 *                                   possible.                            
 * @param      inProgramId           This is the name of the program where
 *                                   the fatal error occurred.            
 * @param      inSubprocedureName    This is the name of the (sub)procedur
 *                                   (or subroutine) where the fatal error
 *                                   occurred.                            
 * @param      inBatchId             This is the id of the batch that was 
 *                                   being processed when the fatal error 
 *                                   occurred.                            
 * @param      inOverrideEMailMessageId                                   
 *                                   This is the id of the message to be  
 *                                   used as to source of the text for the
 *                                   e-mail to be sent.                   
 * @param      inErrorMessageId      This is the id of the message        
 *                                   associated with the specific fatal   
 *                                   condition.                           
 * @param      inMessageText         This is the message text to be       
 *                                   written to the Program Diagnostic    
 *                                   Table (UPGMDIAG).                    
 * =======================================================================
 * - Possible Completion Message Ids -                                    
 * ===================================                                    
 * @throws     none     No return from this procedure is possible.        
 * =======================================================================
 */
@Service
public class TerminateBatchActivationGroupService extends BaseErrorNotificationMethodsService {

    private static final String SUBPROCEDURE_NAME = "terminateBatchActivationGroup";
    private static final String DSPJOBLOG = "DSPJOBLOG JOB(*) OUTPUT(*PRINT)";

    @Autowired
    private ApplicationContext appCtx;

    @Autowired
    private ProgramStatusDataStructure programStatusDataStructure;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private FailProcessEntryService failProcessEntryService;

    @Autowired
    private IsCurrentProcessStoredProcedureService isCurrentProcessStoredProcedureService;

    @Autowired
    private IsCurrentProcessEntryCriticalService isCurrentProcessEntryCriticalService;

    @Autowired
    private SetCurrentProcessCompletionMessageIdService setCurrentProcessCompletionMessageIdService;

    @Autowired
    private EndActivationGroupService endActivationGroupService;

    @Autowired
    private PutJobInformationMessageService putJobInformationMessageService;

    @Autowired
    private PutMessageSystemOperatorMessageQueueService putMessageSystemOperatorMessageQueueService;

    @Autowired
    private GetThreadJobDataTableAttributesService getThreadJobDataTableAttributesService;

    @Autowired
    private SendDataQueueMessageService sendDataQueueMessageService;

    public void execute(TerminateBatchActivationGroupContext context) {
        try {
            // ======================================================================
            // Log the error to the Program Diagnostics Table (UPGMDIAG).
            // ======================================================================
            PutProgramDiagnosticsContext putProgramDiagnosticsContext = PutProgramDiagnosticsContext.builder()
                    .inProgramName(context.getInProgramId())
                    .inMessageId(context.getInErrorMessageId())
                    .inMessageText(context.getInMessageText())
                    .inSubprocedureName(context.getInSubprocedureName())
                    .build();

            // ======================================================================
            // Retrieve the CREATE_TIMESTAMP value from the Program Diagnostics
            // (UPGMDIAG) entry just added.  This value will be used as the
            // CREATE_TIMESTAMP value for the Job Log (UJOBLOG) entries.
            // ======================================================================
            try {
            context.setCreateTimestamp(
                    Optional.ofNullable(
                            jdbcTemplate.query(
                                    new StringBuilder()
                                            .append("select ifnull( max(CREATE_TIMESTAMP), current_timestamp) ")
                                            .append("from   UPGMDIAG                                          ")
                                            .append("where  PROGRAM_NAME = ?                                  ")
                                            .toString(),
                                    (ps) -> ps.setString(1, context.getInProgramId()),
                                    (ResultSet rs) -> rs.getTimestamp(0))
                                    .toLocalDateTime())
                            .orElse(LocalDateTime.of(1, 1, 1, 0, 0, 0)));
                context.setSqlCode(SQL_SUCCESSFUL);
            } catch (EmptyResultDataAccessException e) {
                context.setSqlCode(SQL_ROW_NOT_FOUND);
            } catch (DataAccessException e) {
                context.setSqlCode(SQL_SYSTEM_ERROR);
            }

            if (context.getSqlCode() != SQL_SUCCESSFUL) {
                //    Log error but continue processing.
                // TODO: Unsupported Instruction -> %addr(SQLCA)
                PutSqlDiagnosticsContext putSqlDiagnosticsContext = PutSqlDiagnosticsContext.builder()
                        .inProgramName(programStatusDataStructure.getProgramName())
                        .inSubprocedureName(SUBPROCEDURE_NAME)
                        .inHostVariables(format("inProgramId=%s", context.getInProgramId()))
                        .inSqlCaPointer("")
                        .build();
                context.setCreateTimestamp(LocalDateTime.now());
            }

            // ======================================================================
            // Issue command 'DSPJOBLOG JOB(*) OUTPUT(*PRINT)', which will cause the
            // job log to be output to a spooled file (which hopefully won't be
            // deleted before development can review it).
            // ======================================================================
            context.setCommand(DSPJOBLOG);

            try {
                // TODO: Unsupported Instruction -> qcmdexc (command : %len(command) )
            }
            catch (Exception e) {
                //    ignore error
            }

            // ======================================================================
            // Create a dynamic SQL statement for inserting the job log data for the
            // current job into the Job Log Table (UJOBLOG) for subsequent analysis.
            // ======================================================================
            // TODO: Needs to be re-engineered
            //    dynamicSQLString  =
            //       'insert into UJOBLOG'
            //    +        ' ( CREATE_TIMESTAMP,  JOB_NAME,          JOB_NUMBER,'
            //    +          ' JOB_USER_ID,       ORDINAL_POSITION,  MESSAGE_ID,'
            //    +          ' MESSAGE_TYPE,      MESSAGE_SUBTYPE,   SEVERITY,'
            //    +          ' MESSAGE_TIMESTAMP, FROM_LIBRARY,      FROM_PROGRAM,'
            //    +          ' FROM_MODULE,       FROM_PROCEDURE,    FROM_INSTRUCTION,'
            //    +          ' TO_LIBRARY,        TO_PROGRAM,        TO_MODULE,'
            //    +          ' TO_PROCEDURE,      TO_INSTRUCTION,    FROM_USER,'
            //    +          ' MESSAGE_LIBRARY,   MESSAGE_FILE,  MESSAGE_TOKEN_LENGTH,'
            //    +          ' MESSAGE_TOKENS,    MESSAGE_TEXT,'
            //    +          ' MESSAGE_SECOND_LEVEL_TEXT'
            //    +        ' )'
            //    + ' select timestamp(''' + %char(createTimestamp)
            //    +                          ''') as CREATE_TIMESTAMP,'
            //    +          ' ''' + %trim (sdJobName) + ''''
            //    +                             ' as JOB_NAME,'
            //    +          ' '   + %editc(sdJobNumber : 'X')
            //    +                             ' as JOB_NUMBER,'
            //    +          ' ''' + %trim (sdJobUser) + ''''
            //    +                             ' as JOB_USER_ID,'
            //    +          ' ORDINAL_POSITION,  MESSAGE_ID,'
            //    +          ' MESSAGE_TYPE,      MESSAGE_SUBTYPE,   SEVERITY,'
            //    +          ' MESSAGE_TIMESTAMP, FROM_LIBRARY,      FROM_PROGRAM,'
            //    +          ' FROM_MODULE,       FROM_PROCEDURE,    FROM_INSTRUCTION,'
            //    +          ' TO_LIBRARY,        TO_PROGRAM,        TO_MODULE,'
            //    +          ' TO_PROCEDURE,      TO_INSTRUCTION,    FROM_USER,'
            //    +          ' MESSAGE_LIBRARY,   MESSAGE_FILE,  MESSAGE_TOKEN_LENGTH,'
            //    +          ' case when MESSAGE_TOKEN_LENGTH > 512'
            //    +               ' then substr(MESSAGE_TOKENS, 1, 512)'
            //    +               ' else        MESSAGE_TOKENS'
            //    +          ' end,'
            //    +          ' cast (MESSAGE_TEXT as varchar(4096) CCSID 37)'
            //    +                             ' as MESSAGE_TEXT,'
            //    +          ' cast (MESSAGE_SECOND_LEVEL_TEXT'
            //    +                             ' as varchar(4096) CCSID 37)'
            //    +                             ' as MESSAGE_SECOND_LEVEL_TEXT'
            //    + ' from     table( QSYS2.JOBLOG_INFO('''
            //    +                  %editc(sdJobNumber : 'X') + '/'
            //    +                  %trim (sdJobUser)         + '/'
            //    +                  %trim (sdJobName)
            //    +                                     ''')'
            //    +               ' ) as JOBLOG'
            //    + ' with NC';
            //    exec sql
            //       execute immediate :dynamicSQLString;
            //    if sqlCode <> SQL_SUCCESSFUL;
            //    Log error but continue processing.
            //       putSQLDiagnostics (sdProgram : SUBPROCEDURE_NAME + ' 2' :
            //                          dynamicSQLString : %addr(SQLCA) );
            //    endif;                                                               

            // ======================================================================
            // Retrieve Environment-Scope Attributes for Batch Error Notification.
            // ======================================================================
            GetErrorNotificationAttributesContext getErrorNotificationAttributesContext =
                    GetErrorNotificationAttributesContext.builder()
                            .build();
            context.setDsErrorNotification(
                    getErrorNotificationAttributesService.execute(getErrorNotificationAttributesContext));
            context.setMessageId(getErrorNotificationAttributesContext.getOutMessageId());

            if (context.getMessageId() == BLANK)
                context.setIcom400Available(context.getDsErrorNotification().getEnICOM400Available());
            else
                context.setIcom400Available(false);

            context.setErrorNotificationProgram(context.getDsErrorNotification().getEnEMailNotificationProgram());

            // ======================================================================
            // Execute e-mail notification if available.
            // ======================================================================
            if (context.getIcom400Available() || !isBlank(context.getErrorNotificationProgram())) {
                //    ===================================================================
                //    Get secondary message text for the body of the e-mail.
                //    ===================================================================
                if (context.getInOverrideEmailMessageId() == BLANK &&
                        context.getDsErrorNotification().getEnDefaultEMailMessageId() == BLANK)
                    context.setEmailMessageId(ERROR_UNKNOWN_INTERFACE_ERROR);
                else if (context.getInOverrideEmailMessageId() == BLANK &&
                        context.getDsErrorNotification().getEnDefaultEMailMessageId() != BLANK)
                    context.setEmailMessageId(context.getDsErrorNotification().getEnDefaultEMailMessageId());
                else if (context.getInOverrideEmailMessageId() != BLANK)
                    context.setEmailMessageId(context.getInOverrideEmailMessageId());

                GetExpandedMessageTextSecondaryContext getExpandedMessageTextSecondaryContext =
                        GetExpandedMessageTextSecondaryContext.builder()
                                .inMessageId(context.getEmailMessageId())
                                .inLanguage(ENGLISH)
                                .inMessageData(String.valueOf(context.getInBatchId()))
                                .build();
                context.setEmailMessageText(
                        getExpandedMessageTextSecondaryService.execute(getExpandedMessageTextSecondaryContext));

                //    ===================================================================
                //    Send E-Mail Notification.
                //    ===================================================================
                if (context.getIcom400Available() && isBlank(context.getErrorNotificationProgram())) {
                    //       ================================================================
                    //       Retrieve the Error Notification Distribution Group.
                    //       ================================================================
                    try {
                        jdbcTemplate.query(
                                new StringBuilder()
                                        .append("select     a.PROCESS_ID,                          ")
                                        .append("           b.PROCESS_TYPE,                        ")
                                        .append("           ifnull(c.ERROR_NOTIFICATION_GROUP, '') ")
                                        .append("from       QTEMP/PROCESSID  a                     ")
                                        .append("inner join UPRCENT  b                             ")
                                        .append("on         b.PROCESS_ID   = a.PROCESS_ID          ")
                                        .append("inner join UPRCTYP  c                             ")
                                        .append("on         c.PROCESS_TYPE = b.PROCESS_TYPE        ")
                                        .toString(),
                                (rs) -> {
                                    context.setProcessId(new ProcessId(rs.getInt(1)));
                                    context.setProcessType(rs.getString(2));
                                    context.setErrorNotificationDistributionGroup(rs.getString(3));
                                });
                        context.setSqlCode(SQL_SUCCESSFUL);
                    } catch (EmptyResultDataAccessException e) {
                        context.setSqlCode(SQL_ROW_NOT_FOUND);
                    } catch (DataAccessException e) {
                        context.setSqlCode(SQL_SYSTEM_ERROR);
                    }

                    if (context.getSqlCode() != SQL_SUCCESSFUL) {
                        //          Log error but continue processing.
                        // TODO: Unsupported Instruction -> %addr(SQLCA)
                        PutSqlDiagnosticsContext putSqlDiagnosticsContext = PutSqlDiagnosticsContext.builder()
                                .inProgramName(programStatusDataStructure.getProgramName())
                                .inSubprocedureName(format("%s 3", SUBPROCEDURE_NAME))
                                .inHostVariables(format("inProgramId=%s", context.getInProgramId()))
                                .build();
                        putSqlDiagnosticsService.execute(putSqlDiagnosticsContext);
                    }

                    if (!isBlank(context.getErrorNotificationDistributionGroup())) {
                        context.setEmailMessageText(
                                format("Process Type=%s, Process Id=%s, Job=%s/%s/%s: %s",
                                        trim(context.getProcessType()),
                                        trim(String.valueOf(context.getProcessId())),
                                        trim(programStatusDataStructure.getJobName()),
                                        programStatusDataStructure.getJobUser(),
                                        programStatusDataStructure.getJobNumber(),
                                        context.getEmailMessageText()));

                        //          =============================================================
                        //          Use ICOM/400 to send an e-mail to the application group.
                        //          =============================================================
                        SendEmailMessageIcmContext sendEmailMessageIcmContext = SendEmailMessageIcmContext.builder()
                                .distributionName(context.getErrorNotificationDistributionGroup())
                                .messageText(context.getEmailMessageText())
                                .build();
                        sendEmailMessageIcmService.execute(sendEmailMessageIcmContext);
                    }
                    else if (!isBlank(context.getErrorNotificationProgram())) {
                        //       ================================================================
                        //       Use custom routine to send an e-mail.
                        //       ================================================================
                        sendEmailMessageCustom(context);
                    }
                }
            }

            // ======================================================================
            // Update the current Process Entry (UPRCENT), setting a "FAILED" status.
            // ======================================================================
            FailProcessEntryContext failProcessEntryContext = FailProcessEntryContext.builder()
                    .build();
            failProcessEntryService.execute(failProcessEntryContext);

            // ======================================================================
            // If invoked within a Stored Procedure call, bypass the QSYSOPR message.
            // If current process type is not critical, bypass the QSYSOPR message.
            // ======================================================================
            IsCurrentProcessStoredProcedureContext isCurrentProcessStoredProcedureContext =
                    IsCurrentProcessStoredProcedureContext.builder().build();
            IsCurrentProcessEntryCriticalContext isCurrentProcessEntryCriticalContext =
                    IsCurrentProcessEntryCriticalContext.builder().build();
            if (isCurrentProcessStoredProcedureService.execute(isCurrentProcessStoredProcedureContext) ||
                    !isCurrentProcessEntryCriticalService.execute(isCurrentProcessEntryCriticalContext)) {
                SetCurrentProcessCompletionMessageIdContext setCurrentProcessCompletionMessageIdContext =
                        SetCurrentProcessCompletionMessageIdContext.builder()
                                .inMessageId(context.getInErrorMessageId())
                                .build();
                setCurrentProcessCompletionMessageIdService.execute(setCurrentProcessCompletionMessageIdContext);
                context.setMessageIdOut(setCurrentProcessCompletionMessageIdContext.getOutMessageId());
                EndActivationGroupContext endActivationGroupContext = EndActivationGroupContext.builder()
                        .build();
                endActivationGroupService.execute(endActivationGroupContext);
                return; // This RETURN would never execute.
            }

            // ======================================================================
            // Send a Job Information message to the System Operator Message Queue.
            // ======================================================================
            PutJobInformationMessageContext putJobInformationMessageContext = PutJobInformationMessageContext.builder()
                    .build();
            putJobInformationMessageService.execute(putJobInformationMessageContext);
            context.setMessageId(putJobInformationMessageContext.getOutMessageId());

            // ======================================================================
            // Send a message to the System Operator Message Queue.
            // ----------------------------------------------------------------------
            // Note that "C" is the only reply accepted by message id GBL0074,
            // therefore there isn't any need to test the value contained in "reply".
            // ======================================================================
            PutMessageSystemOperatorMessageQueueContext putMessageSystemOperatorMessageQueueContext =
                    PutMessageSystemOperatorMessageQueueContext.builder()
                            .inProgramId(context.getInProgramId())
                            .inSubprocedureName(context.getInSubprocedureName())
                            .inMessageId("")
                            .inMessageData("")
                            .build();
            context.setReply(
                    putMessageSystemOperatorMessageQueueService.execute(putMessageSystemOperatorMessageQueueContext));

            // Get thread job attributes
            GetThreadJobDataTableAttributesContext getThreadJobDataTableAttributesContext =
                    GetThreadJobDataTableAttributesContext.builder().build();
            context.setDsThreadJob(
                    getThreadJobDataTableAttributesService.execute(getThreadJobDataTableAttributesContext));
            context.setMessageId(getThreadJobDataTableAttributesContext.getOutMessageId());

            // If thread job data exists, then send failed message to the
            // data queue.
            if (context.getMessageId() == BLANK && !isBlank(context.getDsThreadJob().getTjQName())) {
                //    Set Data Queue Parameters
                context.setDqTask(context.getDsThreadJob().getTjTaskName());
                context.setDqJob(context.getDsThreadJob().getTjJobName());
                context.setDqStatus("ABNORMAL");

                context.setSndQLibrary(context.getDsThreadJob().getTjQLibrary());
                context.setSndQName(context.getDsThreadJob().getTjQName());
                context.setSndData(context.getDataQData());
                context.setSndKeyData(context.getDqTask());

                //    Send job status to ThreadQ
                SendDataQueueMessageContext sendDataQueueMessageContext = SendDataQueueMessageContext.builder()
                        .dataQueueName(context.getSndQName())
                        .dataQueueLibrary(context.getSndQLibrary())
                        .dataQueueLength(context.getSndDataLen())
                        .data(context.getSndData())
                        .keyLength(context.getSndKeyLen())
                        .keyData(context.getSndKeyData())
                        .build();
                sendDataQueueMessageService.execute(sendDataQueueMessageContext);
            }

            EndActivationGroupContext endActivationGroupContext = EndActivationGroupContext.builder()
                    .build();
            endActivationGroupService.execute(endActivationGroupContext);
            return;  // This RETURN would never execute, but a procedure must have a 'RETURN'.
        }
        catch (Exception e) {
            EndActivationGroupContext endActivationGroupContext = EndActivationGroupContext.builder()
                    .build();
            endActivationGroupService.execute(endActivationGroupContext);
        }
    }

    private void sendEmailMessageCustom(TerminateBatchActivationGroupContext context) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Object sendEmailMessageCustom = appCtx.getBean(context.getErrorNotificationProgram());
        Method method = sendEmailMessageCustom.getClass()
                .getMethod("execute", SendEmailMessageCustomContext.class);
        SendEmailMessageCustomContext sendEmailMessageCustomContext = SendEmailMessageCustomContext.builder()
                .emailAddress(context.getDsErrorNotification().getEnNotificationEMailAddress())
                .messageText(context.getEmailMessageText())
                .build();
        method.invoke(sendEmailMessageCustom, sendEmailMessageCustomContext);
    }
}
