package com.vertex.mrdownload.global.errornotificationmethods.geterrornotificationattributes;

import com.vertex.mrdownload.caps.tasktypemethods.BaseTaskTypeMethodsService;
import com.vertex.mrdownload.global.errornotificationmethods.ErrorNotification;
import org.springframework.stereotype.Service;

/**
 * =======================================================================
 * @subprocedure getErrorNotificationAttributes
 * @module       GBLERRNTF0
 * =======================================================================
 * @purpose    This procedure will return environment-scope error
 *             notification attributes.
 * =======================================================================
 * - Constraints / Assertions -
 * ============================
 * @assertion  An entry must exist in the Global Environment-Scope
 *             Attribute Table (UGLOBAL).
 * =======================================================================
 * - Parameters -
 * ==============
 * @return     <typeErrorNotificationDS_GBLERRNTFY>
 *                                   This is a data structure containing
 *                                   the environment-scope attribute value
 *                                   for batch error notification.
 * @param      outMessageId          In the event that an error is
 *                                   encountered during execution of this
 *                                   procedure, this parameter will
 *                                   contain the associated message id.
 * =======================================================================
 * - Possible Completion Message Ids -
 * ===================================
 * @throws     GBL0001  Subprocedure interface/processing error was
 *                      encountered.
 * @throws     GBL0002  Database Access Error was encountered.
 * @throws     GBL0090  Global Environment-Scope Attributes not found
 * =======================================================================
 */
@Service
public class GetErrorNotificationAttributesService extends BaseTaskTypeMethodsService {

    public ErrorNotification execute(GetErrorNotificationAttributesContext context) {
        // TODO: Implementation is missing

        return ErrorNotification.builder()
                .build();
    }
}
