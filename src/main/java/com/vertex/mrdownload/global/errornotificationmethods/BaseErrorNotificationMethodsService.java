package com.vertex.mrdownload.global.errornotificationmethods;

import com.vertex.mrdownload.BaseService;
import com.vertex.mrdownload.global.errornotificationmethods.geterrornotificationattributes.GetErrorNotificationAttributesService;
import com.vertex.mrdownload.global.errornotificationmethods.sendemailmessageicm.SendEmailMessageIcmService;
import com.vertex.mrdownload.global.messagehandlingmethods.getexpandedmessagetextsecondary.GetExpandedMessageTextSecondaryService;
import com.vertex.mrdownload.global.sqlerrorhandlingmethods.putsqldiagnostics.PutSqlDiagnosticsService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * =======================================================================
 * © 2008 Vertex Business Services Holdings LLC
 * =======================================================================
 * @systemId       eCIS+:  Global Package
 * @memberName     GBLERRNTFY
 * @memberTitle    Error Notification Methods
 * @author         Jeff Haddix (OCJEF01)
 * @dateWritten    07/10/2007
 * =======================================================================
 * @purpose
 *     This Service Program contains procedures for collecting diagnostic
 *     information and error notification.
 * =======================================================================
 *     The following is the list of procedures defined in this copy member
 *
 *         getErrorNotificationAttributes              (module GBLERRNTF0)
 *
 *         putTriggerEscapeMessage                     (module GBLERRNTF1)
 *
 *         putMessageSystemOperatorMessageQueue        (module GBLERRNTF2)
 *         terminateBatchActivationGroup               (module GBLERRNTF2)
 *
 *         sendEMailMessageICM                   (CLLE program GBLSNDMAIL)
 * =======================================================================
 * Compilation / Binding Instructions:
 *     1)  Prototype Definition Copy Member - Do not compile
 *     2)  Use MKS Object Type RPGLEPR (RPGLE Prototype Definition)
 * =======================================================================
 * Modifications:
 *
 * Date        Jira Task Id     DT#    DR#    Version     Developer
 * ----------  ---------------  -----  -----  ----------  ----------------
 * 07/10/2007  210848  n/a      50462  51388  NextGen R1  Jeff Haddix
 * Initial Version
 *
 * 09/24/2007  209306  215760   51685  51685  NextGen R1  David Swartzendr
 * Add prototype definition for external CL program GBLSNDMAIL
 *
 * 04/14/2008  233338  Q11265   52608  52608  NextGen R2  Jeff Haddix
 * Add subprocedure terminateBatchActivationGroup.
 *
 * 11/17/2008  246640  Q11432   53333  53334  eCIS+ V2R1  Jeff Haddix
 * 1) Add Type Definition typeErrorNotificationDS_GBLERRNTFY.
 * 2) Add Named Constant for Error Message GBL0090.
 * 3) Add procedure getErrorNotificationAttributes_GBLERRNTFY.
 *
 * 03/26/2009  AGST-932         53936  53949  eCIS+ V2R1  Jeff Haddix
 * Remove the logic from putTriggerEscapeMessage that limits execute to
 * interactive mode.
 *
 * 02/14/2011  Jira ECIS-1244   56198  56198  eCIS+ V3R2  Jeff Haddix
 * Add @assertion to terminateBatchActivationGroup.
 *
 * 07/02/2015  Jira ECIS-24668  62976  62977  eCIS+ V3R2  Jeff Haddix
 * 07/02/2015  Jira ECIS-24668  62976  62979  eCIS+ V3R3  Jeff Haddix
 * Add @assertion to terminateBatchActivationGroup.
 * =======================================================================
 */
public abstract class BaseErrorNotificationMethodsService extends BaseService {

    protected static final int SQL_SUCCESSFUL = 0;
    protected static final int SQL_END_OF_CURSOR = 100;
    protected static final int SQL_ROW_NOT_FOUND = 100;
    protected static final int SQL_OBJECT_NOT_FOUND = -204;
    protected static final int SQL_INDICATOR_VARIABLE_REQUIRED = -305;
    protected static final int SQL_CURSOR_NOT_OPEN = -501;
    protected static final int SQL_CURSOR_ALREADY_OPEN = -502;
    protected static final int SQL_REFERENTIAL_INTEGRITY_INSERT_ERROR = -530;
    protected static final int SQL_REFERENTIAL_INTEGRITY_UPDATE_ERROR = -531;
    protected static final int SQL_REFERENTIAL_INTEGRITY_DELETE_ERROR = -532;
    protected static final int SQL_CHECK_CONSTRAINT_VIOLATION = -545;
    protected static final int SQL_TABLE_ALREADY_EXISTS = -601;
    protected static final int SQL_DUPLICATE_KEY = -803;
    protected static final int SQL_MULTIPLE_ROWS_RETURNED = -811;
    protected static final int SQL_SYSTEM_ERROR = -901;
    protected static final int SQL_DATA_CHANGE_VIOLATION = -907;
    protected static final int SQL_TABLE_ROW_IN_USE = -913;

    // -----------------------------------------------------------------------
    // - Global Language Set Codes -
    // -----------------------------------------------------------------------
    protected static final String ENGLISH = "ENG";
    protected static final String FRENCH = "FRN";
    protected static final String SPANISH = "SPN";

    @Autowired
    protected GetErrorNotificationAttributesService getErrorNotificationAttributesService;

    @Autowired
    protected GetExpandedMessageTextSecondaryService getExpandedMessageTextSecondaryService;

    @Autowired
    protected PutSqlDiagnosticsService putSqlDiagnosticsService;

    @Autowired
    protected SendEmailMessageIcmService sendEmailMessageIcmService;

    public BaseErrorNotificationMethodsService() {
        super("GLBERRNTFY");
    }
}
