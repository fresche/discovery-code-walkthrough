package com.vertex.mrdownload.global.errornotificationmethods.putmessagesystemoperatormessagequeue;

import com.vertex.mrdownload.caps.tasktypemethods.BaseTaskTypeMethodsService;
import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.Switch;
import org.springframework.stereotype.Service;

/**
 * =======================================================================
 * @subprocedure putMessageSystemOperatorMessageQueue
 * =======================================================================
 * @purpose    This subprocedure will put a message to the System Operator
 *             Message Queue (QSYS/QSYSOPR).
 * =======================================================================
 */
@Service
public class PutMessageSystemOperatorMessageQueueService extends BaseTaskTypeMethodsService {

    public Switch execute(PutMessageSystemOperatorMessageQueueContext context) {
        // TODO: Implementation is missing

        return new Switch("");
    }
}
