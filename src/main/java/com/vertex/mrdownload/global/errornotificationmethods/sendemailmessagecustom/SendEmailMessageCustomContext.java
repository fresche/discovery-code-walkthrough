package com.vertex.mrdownload.global.errornotificationmethods.sendemailmessagecustom;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SendEmailMessageCustomContext {

    private String emailAddress;
    private String messageText;
}
