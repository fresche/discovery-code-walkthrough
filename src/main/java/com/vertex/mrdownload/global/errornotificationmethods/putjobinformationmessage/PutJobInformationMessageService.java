package com.vertex.mrdownload.global.errornotificationmethods.putjobinformationmessage;

import com.vertex.mrdownload.caps.tasktypemethods.BaseTaskTypeMethodsService;
import com.vertex.mrdownload.global.errornotificationmethods.ErrorNotification;
import org.springframework.stereotype.Service;

/**
 * =======================================================================
 * @subprocedure putJobInformationMessage
 * =======================================================================
 * @purpose    This subprocedure will put a Job Information message to the
 *             System Operator Message Queue (QSYS/QSYSOPR).
 * =======================================================================
 * - Constraints / Assertions -
 * ============================
 * @assertion  Message id GBL0104 will be used to communicate the job
 *             information.
 * =======================================================================
 */
@Service
public class PutJobInformationMessageService extends BaseTaskTypeMethodsService {

    public void execute(PutJobInformationMessageContext context) {
        // TODO: Implementation is missing
    }
}
