package com.vertex.mrdownload.global.programerrorhandlingmethods.putprogramdiagnostics;

import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class PutProgramDiagnosticsContext {

    // Parameters
    private String inProgramName;
    private MessageId inMessageId;
    private String inMessageText;
    private String inSubprocedureName;
}
