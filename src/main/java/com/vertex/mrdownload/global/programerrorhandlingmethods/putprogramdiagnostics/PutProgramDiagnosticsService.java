package com.vertex.mrdownload.global.programerrorhandlingmethods.putprogramdiagnostics;

import com.vertex.mrdownload.caps.tasktypemethods.BaseTaskTypeMethodsService;
import com.vertex.mrdownload.global.programerrorhandlingmethods.BaseProgramErrorHandlingMethodsService;
import org.springframework.stereotype.Service;

/**
 * =======================================================================
 * @subprocedure putProgramDiagnostics
 * @module       GBLPGMERR
 * =======================================================================
 * @purpose    This subprocedure will store diagnostic information
 *             associated with the anomalous completion of an RPGLE
 *             Subprocedure.  This information is stored into the Program
 *             Diagnostics Table (UPGMDIAG) for subsequent analysis.
 * =======================================================================
 * - Constraints / Assertions -
 * ============================
 * @assertion  It is expected that an SQL query will be used to review the
 *             information stored by this subprocedure.
 *             Example:  SELECT   * FROM UPGMDIAG
 *                       ORDER BY 1 DESC
 * @assertion  It is advised that the invoking routine execute the DUMP
 *             op-code in association with execution of this subprocedure.
 *             Subsequently, it should be possible for a programmer to
 *             match the UPGMDIAG entry with the dump, based upon
 *             CREATE_TIMESTAMP and CREATED_BY_USER.
 * =======================================================================
 * - Parameters -
 * ==============
 * @return     n/a
 * @param      inProgramName         This is the name of the module that
 *                                   was executing when the error was
 *                                   encountered.
 * @param      inMessageId           This is the id of the message issued
 *                                   for the error condition.  Typically,
 *                                   this will be either a CPFxxxx or
 *                                   MCHxxxx message id.
 * @param      inMessageText         This is the text associated with the 
 *                                   message id.
 * @param      inSubprocedureName    This is the name of the subprocedure
 *                                   (or subroutine) that was executing
 *                                   when the error was encountered.  If
 *                                   invoked from the mainline of a "Main"
 *                                   module, "Mainline" should be passed.
 * =======================================================================
 * @throws     n/a                  As an error-handling method, if a
 *                                  processing error is encountered while
 *                                  handling an error from the calling
 *                                  routine, a hard halt (MSGW) is the
 *                                  only appropriate response.
 * =======================================================================
 */
@Service
public class PutProgramDiagnosticsService extends BaseProgramErrorHandlingMethodsService {

    public void execute(PutProgramDiagnosticsContext context) {
        // TODO: Implementation is missing
    }
}
