package com.vertex.mrdownload.global.programerrorhandlingmethods;

import com.vertex.mrdownload.BaseService;

/**
 * =======================================================================
 * (C) 2002-Orcom Solutions, Inc. All Rights Reserved
 * Orcom Solutions is an Alliance Data Systems company
 * =======================================================================
 * @systemId       E-CIS: GLOBAL Package
 * @memberName     GBLPGMERR
 * @memberTitle    Prototype Definitions - Program Error-Handling Methods
 * @author         Jeff Haddix (OCJEF01)
 * @dateWritten    10/06/2006
 * =======================================================================
 * @purpose
 *     This member contains Prototype Definitions (PR) for methods used
 *     for RPGLE Subprocedure Error-Handling.
 * =======================================================================
 *     The following is the list of subprocedures defined in this copy
 *     member.
 *
 *         putProgramDiagnostics                  (module  GBLPGMERR)
 * =======================================================================
 * Compilation / Binding Instructions:
 *     1)  Prototype Definition Copy Member - Do not compile
 *     2)  Use MKS Object Type RPGLEPR (RPGLE Prototype Definition)
 * =======================================================================
 * Modifications:
 *
 * Date        SI/SVI  QA/Task  DT#    DR#    Version     Developer
 * ----------  ------  -------  -----  -----  ----------  ----------------
 * 10/06/2006  193588  Q10623   50462  50462  NextGen R1  Jeff Haddix
 * Initial Version
 * =======================================================================
 */
public abstract class BaseProgramErrorHandlingMethodsService extends BaseService {

    public BaseProgramErrorHandlingMethodsService() {
        super("GLBPGMERR");
    }
}
