package com.vertex.mrdownload.global.messagehandlingmethods.getexpandedmessagetextsecondary;

import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GetExpandedMessageTextSecondaryContext {

    // Parameters
    private MessageId inMessageId;
    private String inLanguage;
    private String inMessageData;
    private MessageId outMessageId;
}
