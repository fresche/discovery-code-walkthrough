package com.vertex.mrdownload.global.messagehandlingmethods;

import com.vertex.mrdownload.BaseService;
import com.vertex.mrdownload.global.errornotificationmethods.geterrornotificationattributes.GetErrorNotificationAttributesService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * =======================================================================
 * (C) 2002-Orcom Solutions, Inc. All Rights Reserved
 * Orcom Solutions is an Alliance Data Systems company
 * =======================================================================
 * @systemId       E-CIS: GLOBAL Package
 * @memberName     GBLMESSAGE
 * @memberTitle    Prototype Definitions - Message-Handling Methods
 * @author         Jeff Haddix (OCJEF01)
 * @dateWritten    09/25/2007
 * =======================================================================
 * @purpose
 *     This member contains Prototype Definitions (PR) for methods used
 *     to handle message text.
 * =======================================================================
 *     The following is the list of subprocedures defined in this copy
 *     member.
 *
 *         getExpandedMessageText                 (module GBLMESSAGE)
 *         getExpandedMessageTextSecondary        (module GBLMESSAGE)
 * =======================================================================
 * Compilation / Binding Instructions:
 *     1)  Prototype Definition Copy Member - Do not compile
 *     2)  Use MKS Object Type RPGLEPR (RPGLE Prototype Definition)
 * =======================================================================
 * Modifications:
 *
 * Date        SI/SVI  QA/Task  DT#    DR#    Version     Developer
 * ----------  ------  -------  -----  -----  ----------  ----------------
 * 09/25/2007  209306  215760   51685  51685  NextGen R1  Jeff Haddix
 * Initial Version
 * =======================================================================
 */
public abstract class BaseMessageHandlingMethodsService extends BaseService {

    public BaseMessageHandlingMethodsService() {
        super("GLBMESSAGE");
    }
}
