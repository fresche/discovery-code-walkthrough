package com.vertex.mrdownload.global.commitmentcontrolmethods.commitchanges;

import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CommitChangesContext {

    // Parameters
    private MessageId outMessageId;
}
