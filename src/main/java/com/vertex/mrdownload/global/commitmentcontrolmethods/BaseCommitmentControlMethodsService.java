package com.vertex.mrdownload.global.commitmentcontrolmethods;

import com.vertex.mrdownload.BaseService;

/**
 * =======================================================================
 * © 2008 Vertex Business Services Holdings LLC                           
 * =======================================================================
 * @systemId       eCIS+:  Global Package                                 
 * @memberName     GBLCOMMIT                                              
 * @memberTitle    Commitment Control Methods                             
 * @author         Jeff Haddix (OCJEF01)                                  
 * @dateWritten    11/02/2006                                             
 * =======================================================================
 * @purpose                                                               
 *     This Service Program contains procedures used for Commitment       
 *     Control.                                                           
 * =======================================================================
 *     The following is the list of procedures defined in this copy member
 *                                                                        
 *         buildIsolationLevelClause              (module GBLCOMMIT)      
 *         commitChanges                          (module GBLCOMMIT)      
 *         rollbackChanges                        (module GBLCOMMIT)      
 * =======================================================================
 * Compilation / Binding Instructions:                                    
 *     1)  Prototype Definition Copy Member - Do not compile              
 *     2)  Use MKS Object Type RPGLEPR (RPGLE Prototype Definition)       
 * =======================================================================
 * Modifications:                                                         
 *                                                                        
 * Date        Jira Task Id     DT#    DR#    Version     Developer       
 * ----------  ---------------  -----  -----  ----------  ----------------
 * 11/02/2006  193966  T193995  50462  50462  NextGen R1  Jeff Haddix     
 * Initial Version                                                        
 *                                                                        
 * 03/19/2015  Jira ECIS-20133  62667  62667  eCIS+ V3R3  Jeff Haddix     
 * Commitment control errors, while rare, are indicative of severe        
 * database-level damage, which requires prompt response and resolution.  
 * Due to the nature and severity of commitment control errors, such      
 * errors need to always be handled as severe errors, with program        
 * execution being halted and a message sent to the System Operator's     
 * message queue (QSYSOPR) for prompt research and resolution. Therefore, 
 * while the calling routine will typically send a QSYSOPR message and    
 * terminate the program at that point, this decision as to how to handle 
 * such errors should not be left up to the calling routine, nor can it be
 * trusted that the calling routine will handle a commitment control error
 * properly.                                                              
 *                                                                        
 * Therefore, the commitChanges and rollbackChanges procedures have been  
 * modified as follows.                                                   
 *                                                                        
 * In the event of an error, commitChanges and rollbackChanges will:      
 *                                                                        
 * 1. Output an SQL Diagnostic (USQLDIAG) entry to log the information    
 *    pertinent to the error.                                             
 * 2. Send a message to the System Operator (QSYSOPR) message queue.      
 * 3. Terminate the Activation Group.                                     
 *                                                                        
 * The existing outMessageId parameter will be retained, but it will be   
 * changed to a *NOPASS parameter. This will allow existing programs to   
 * use the new version of the commitChanges and rollbackChanges procedures
 * without needing to be modified or recompiled. However, the outMessageId
 * parameter will not be set by the commitChanges and rollbackChanges     
 * procedures. Consequently, whatever value is passed into these          
 * procedures will be returned without change.                            
 *                                                                        
 * For all new development that call the commitChanges and rollbackChanges
 * procedures, the outMessageId parameter should not be coded, and as     
 * existing programs are modified in the future, any calls to the         
 * commitChanges and rollbackChanges procedures should be modified to     
 * remove the outMessageId parameter and the associated obsolete          
 * error-handling.                                                        
 * =======================================================================
 */
public abstract class BaseCommitmentControlMethodsService extends BaseService {

    public BaseCommitmentControlMethodsService() {
        super("GLBSQLERR");
    }
}
