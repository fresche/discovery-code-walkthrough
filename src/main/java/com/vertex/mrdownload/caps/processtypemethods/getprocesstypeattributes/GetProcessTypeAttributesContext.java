package com.vertex.mrdownload.caps.processtypemethods.getprocesstypeattributes;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GetProcessTypeAttributesContext {

    private String inProcessType;
    private String inReportName;
    private String inPrePostProcessCode;
    private String outMessageId;
}