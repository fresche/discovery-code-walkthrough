package com.vertex.mrdownload.caps.processtypemethods.getprocesstypeattributes;

import com.vertex.mrdownload.caps.processtypemethods.BaseProcessTypeMethodsService;
import com.vertex.mrdownload.caps.processtypemethods.ProcessTypeAttributes;
import org.springframework.stereotype.Service;

@Service
public class GetProcessTypeAttributesService extends BaseProcessTypeMethodsService {

    public ProcessTypeAttributes execute(GetProcessTypeAttributesContext context) {
        ProcessTypeAttributes processTypeAttributes = ProcessTypeAttributes.builder()
                .build();

        // TODO: Implementation is missing

        return processTypeAttributes;
    }
}
