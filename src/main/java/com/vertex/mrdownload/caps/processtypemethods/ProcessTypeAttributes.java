package com.vertex.mrdownload.caps.processtypemethods;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProcessTypeAttributes {

    private String pctProcessType;
    private String pctDescription;
    private String pctProcessGroup;
    private String pctStandardProcess;
    private String pctDriverProgram;
    private Boolean pctHasDefaultProcessingProgram;
    private String pctDefaultProcessingProgram;
    private String pctHasBasePreProcessReportDriverProgram;
    private String pctBasePreProcessReportDriverProgram;
    private String pctHasCustomPreProcessReportDriverProgram;
    private String pctCustomPreProcessReportDriverProgram;
    private String pctHasBasePostProcessReportDriverProgram;
    private String pctBasePostProcessReportDriverProgram;
    private String pctHasCustomPostProcessReportDriverProgram;
    private String pctCustomPostProcessReportDriverProgram;
    private String pctJobPriority;
    private String pctClearProcess;
    private String pctSelectProcess;
    private String pctRecalcProcess;
    private String pctPostProcess;
    private String pctPrintProcess;
    private String pctDownloadProcess;
    private String pctUploadProcess;
    private String pctInitiatedViaCommand;
    private String pctHasInitiationCommand;
    private String pctInitiationCommand;
    private String pctBatchIdRequired;
    private String pctBatchDateRequired;
    private String pctPresentationPriority;
    private String pctOverrideJobQueue;
    private String pctDefaultProcessOutcomeId;
}
