package com.vertex.mrdownload.caps.processtypemethods;

import com.vertex.mrdownload.BaseService;

public abstract class BaseProcessTypeMethodsService extends BaseService {

    public BaseProcessTypeMethodsService() {
        super("APSPRCTYP");
    }
}
