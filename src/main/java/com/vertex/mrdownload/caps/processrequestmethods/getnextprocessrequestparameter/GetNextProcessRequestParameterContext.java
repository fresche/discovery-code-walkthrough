package com.vertex.mrdownload.caps.processrequestmethods.getnextprocessrequestparameter;

import com.vertex.mrdownload.caps.packageleveltypedefinitions.ProcessRequestId;
import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GetNextProcessRequestParameterContext {

    // Parameters
    private ProcessRequestId inProcessRequestId;
    private String inDataLabel;
    private MessageId outMessageId;
}
