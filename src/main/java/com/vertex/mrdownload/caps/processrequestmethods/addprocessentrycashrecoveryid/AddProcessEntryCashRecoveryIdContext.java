package com.vertex.mrdownload.caps.processrequestmethods.addprocessentrycashrecoveryid;

import com.vertex.mrdownload.caps.packageleveltypedefinitions.ProcessId;
import com.vertex.mrdownload.caps.packageleveltypedefinitions.ProcessRequestId;
import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AddProcessEntryCashRecoveryIdContext {

    // Parameters
    private ProcessRequestId inProcessRequestId;
    private ProcessId inProcessId;
    private String outCashRecoveryId;
    private MessageId outMessageId;
}
