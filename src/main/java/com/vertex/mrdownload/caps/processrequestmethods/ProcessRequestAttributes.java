package com.vertex.mrdownload.caps.processrequestmethods;

import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.BatchId;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Builder
public class ProcessRequestAttributes {

    private String prProcessRequestId;
    private String prProcessType;
    private String prProcessInitiationMethod;
    private String prRequestedByUser;
    private String prBatchScope;
    private BatchId prBatchId;
    private LocalDate prBatchDate;
    private LocalDateTime prCreationTimestamp;
    private String prInitiationCommand;
}
