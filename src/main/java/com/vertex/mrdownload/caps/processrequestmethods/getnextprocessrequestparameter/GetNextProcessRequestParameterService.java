package com.vertex.mrdownload.caps.processrequestmethods.getnextprocessrequestparameter;

import com.vertex.mrdownload.caps.processentrymethods.ProcessRequestParameterAttributes;
import com.vertex.mrdownload.caps.processrequestmethods.BaseProcessRequestMethodsService;
import org.springframework.stereotype.Service;

@Service
public class GetNextProcessRequestParameterService extends BaseProcessRequestMethodsService {

    public ProcessRequestParameterAttributes execute(GetNextProcessRequestParameterContext context) {
        ProcessRequestParameterAttributes processRequestParameterAttributes =
                ProcessRequestParameterAttributes.builder()
                        .build();

        // TODO: Implementation is missing

        return processRequestParameterAttributes;
    }
}
