package com.vertex.mrdownload.caps.processrequestmethods.deleteprocessrequest;

import com.vertex.mrdownload.caps.packageleveltypedefinitions.ProcessRequestId;
import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DeleteProcessRequestContext {

    // Parameters
    private ProcessRequestId inProcessRequestId;
    private MessageId outMessageId;
}
