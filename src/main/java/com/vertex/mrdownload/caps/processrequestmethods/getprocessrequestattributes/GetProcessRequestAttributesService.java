package com.vertex.mrdownload.caps.processrequestmethods.getprocessrequestattributes;

import com.vertex.mrdownload.caps.processrequestmethods.BaseProcessRequestMethodsService;
import com.vertex.mrdownload.caps.processrequestmethods.ProcessRequestAttributes;
import org.springframework.stereotype.Service;

@Service
public class GetProcessRequestAttributesService extends BaseProcessRequestMethodsService {

    public ProcessRequestAttributes execute(GetProcessRequestAttributesContext context) {
        ProcessRequestAttributes processRequestAttributes =
                ProcessRequestAttributes.builder()
                        .build();

        // TODO: Implementation is missing

        return processRequestAttributes;
    }
}
