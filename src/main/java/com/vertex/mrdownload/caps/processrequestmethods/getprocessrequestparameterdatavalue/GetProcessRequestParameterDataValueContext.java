package com.vertex.mrdownload.caps.processrequestmethods.getprocessrequestparameterdatavalue;

import com.vertex.mrdownload.caps.packageleveltypedefinitions.ProcessRequestId;
import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GetProcessRequestParameterDataValueContext {

    // Parameters
    private ProcessRequestId inProcessRequestId;
    private String inDataLabel;
    private MessageId outMessageId;
}
