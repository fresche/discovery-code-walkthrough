package com.vertex.mrdownload.caps.processrequestmethods.getfirstprocessrequestparameter;

import com.vertex.mrdownload.caps.processentrymethods.ProcessRequestParameterAttributes;
import com.vertex.mrdownload.caps.processrequestmethods.BaseProcessRequestMethodsService;
import org.springframework.stereotype.Service;

@Service
public class GetFirstProcessRequestParameterService extends BaseProcessRequestMethodsService {

    public ProcessRequestParameterAttributes execute(GetFirstProcessRequestParameterContext context) {
        ProcessRequestParameterAttributes processRequestParameterAttributes =
                ProcessRequestParameterAttributes.builder()
                        .build();

        // TODO: Implementation is missing

        return processRequestParameterAttributes;
    }
}
