package com.vertex.mrdownload.caps.processrequestmethods.deleteprocessrequest;

import com.vertex.mrdownload.caps.processrequestmethods.BaseProcessRequestMethodsService;
import org.springframework.stereotype.Service;

/**
 * =======================================================================
 * @subprocedure deleteProcessRequest                                     
 * @module       APSPRCRQS3                                               
 * @author       Teresa Brott (TERESAB)                                   
 * @dateWritten  08/03/2010                                               
 * =======================================================================
 * @purpose    This method will remove the specified Process Request entry
 * =======================================================================
 * - Constraints / Assertions -                                           
 * ============================                                           
 * @assertion  A positive value must be provided for parameter            
 *             inProcessRequestId.                                        
 * @assertion  A matching entry must exist in the Process Request Table   
 *             (UPRCRQS).                                                 
 * =======================================================================
 * - Parameters -                                                         
 * ==============                                                         
 * @return     n/a                                                        
 * @param      inProcessRequestId    This is the id of the Process Request
 *                                   to be removed.                       
 * @param      outMessageId          In the event that an error is        
 *                                   encountered during execution of this 
 *                                   subprocedure, this parameter will    
 *                                   contain the associated message id.   
 * =======================================================================
 * - Possible Completion Message Ids -                                    
 * ===================================                                    
 * @throws     GBL0001  Subprocedure interface/processing error was       
 *                      encountered.                                      
 * @throws     GBL0002  Database Access Error was encountered.            
 * @throws     APS0217  Invalid Process Request Id.                       
 * =======================================================================
 */
@Service
public class DeleteProcessRequestService extends BaseProcessRequestMethodsService {

    public void execute(DeleteProcessRequestContext context) {
        // TODO: Implementation is missing
    }
}
