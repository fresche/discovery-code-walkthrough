package com.vertex.mrdownload.caps.batchthreadmethods.getthreadjobdatatableattributes;

import com.vertex.mrdownload.caps.batchthreadmethods.ThreadJob;
import org.springframework.stereotype.Service;

/**
 * =======================================================================
 * @subprocedure getThreadJobDataTableAttributes                          
 * @module       APSTHREAD1                                               
 * @author       David Swartzendruber                                     
 * @date         10/09/2013                                               
 * =======================================================================
 * @purpose    The procedure will retreive the thread job attributes from 
 *             the termporary job attribute table.                        
 * =======================================================================
 * - Constraints / Assertions -                                           
 * ============================                                           
 * @assertion  The name of the table to retreive from is QTEMP/THREADID.  
 * =======================================================================
 * - Parameters -                                                         
 * ==============                                                         
 * @return     <typeThreadJobDS_APSTHREAD>                                
 *                                   These are the attributes of the      
 *                                   thread job.                          
 * @param      outMessageId          In the event that an error is        
 *                                   encountered during execution of this 
 *                                   subprocedure, this parameter will    
 *                                   contain the associated message id.   
 * =======================================================================
 * - Possible Completion Message Ids -                                    
 * ===================================                                    
 * @throws     GBL0001  Subprocedure interface/processing error was       
 *                      encountered.                                      
 * @throws     GBL0002  Database Access Error was encountered.            
 * @throws     GBL0100  Warning - Row not found.                          
 * =======================================================================
 */
@Service
public class GetThreadJobDataTableAttributesService {

    public ThreadJob execute(GetThreadJobDataTableAttributesContext context) {
        // TODO: Implementation is missing

        return ThreadJob.builder()
                .build();
    }
}
