package com.vertex.mrdownload.caps.batchthreadmethods.getthreadjobdatatableattributes;

import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GetThreadJobDataTableAttributesContext {

    // Parameters
    private MessageId outMessageId;
}
