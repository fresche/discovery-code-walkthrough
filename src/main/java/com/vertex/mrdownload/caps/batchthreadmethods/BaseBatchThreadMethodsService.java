package com.vertex.mrdownload.caps.batchthreadmethods;

import com.vertex.mrdownload.BaseService;

public abstract class BaseBatchThreadMethodsService extends BaseService {

    public BaseBatchThreadMethodsService() {
        super("APSTHREAD");
    }
}
