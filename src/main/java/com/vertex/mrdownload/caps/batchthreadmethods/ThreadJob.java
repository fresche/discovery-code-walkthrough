package com.vertex.mrdownload.caps.batchthreadmethods;

import com.vertex.mrdownload.serviceorder.packageleveltypedefinitions.SequenceNumber;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ThreadJob {

    private String tjQName;
    private String tjQLibrary;
    private String tjTaskName;
    private SequenceNumber tjTaskSequence;
    private String tjJobName;
}
