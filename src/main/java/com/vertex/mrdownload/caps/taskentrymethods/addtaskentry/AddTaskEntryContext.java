package com.vertex.mrdownload.caps.taskentrymethods.addtaskentry;

import com.vertex.mrdownload.caps.packageleveltypedefinitions.TaskId;
import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AddTaskEntryContext {

    private String inTaskType;
    private TaskId outTaskId;
    private MessageId outMessageId;
}
