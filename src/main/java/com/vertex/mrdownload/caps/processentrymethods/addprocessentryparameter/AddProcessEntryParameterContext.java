package com.vertex.mrdownload.caps.processentrymethods.addprocessentryparameter;

import com.vertex.mrdownload.caps.processentrymethods.ProcessEntryParameterAttributes;
import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AddProcessEntryParameterContext {

    // Parameters
    private ProcessEntryParameterAttributes inProcessEntryParameterDs;
    private MessageId outMessageId;
}
