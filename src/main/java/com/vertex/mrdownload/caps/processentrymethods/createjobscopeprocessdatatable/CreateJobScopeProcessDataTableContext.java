package com.vertex.mrdownload.caps.processentrymethods.createjobscopeprocessdatatable;

import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CreateJobScopeProcessDataTableContext {

    // Parameters
    private MessageId outMessageId;
}
