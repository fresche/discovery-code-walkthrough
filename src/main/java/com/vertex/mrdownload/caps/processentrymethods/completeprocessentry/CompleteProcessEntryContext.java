package com.vertex.mrdownload.caps.processentrymethods.completeprocessentry;

import com.vertex.mrdownload.caps.packageleveltypedefinitions.ProcessId;
import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CompleteProcessEntryContext {

    // Parameters
    private ProcessId inProcessId;
    private MessageId outMessageId;
}
