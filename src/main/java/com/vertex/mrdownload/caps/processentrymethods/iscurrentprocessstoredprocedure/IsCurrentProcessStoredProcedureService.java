package com.vertex.mrdownload.caps.processentrymethods.iscurrentprocessstoredprocedure;

import com.vertex.mrdownload.caps.processentrymethods.BaseProcessEntryMethodsService;
import org.springframework.stereotype.Service;

/**
 * =======================================================================
 * @subprocedure isCurrentProcessStoredProcedure                          
 * @module       APSPRCENT1                                               
 * @author       Jeff Haddix (OCJEF01)                                    
 * @dateWritten  11/04/2011                                               
 * =======================================================================
 * @purpose    This method will return a Boolean value indicating whether 
 *             the process currently executing was invoked via a Stored   
 *             Procedure.                                                 
 * =======================================================================
 * - Constraints / Assertions -                                           
 * ============================                                           
 * @assertion  The Stored Procedure indicator is stored in QTEMP/PROCESSID
 * @assertion  This procedure does not return the standard outMessageId   
 *             parameter.  Instead, if an error were to occur during      
 *             execution of this method, a value of *OFF will be returned.
 * =======================================================================
 * - Parameters -                                                         
 * ==============                                                         
 * @return     <BOOLEAN>             This is a Boolean value used to      
 *                                   indicate whether the process         
 *                                   currently executing was invoked via  
 *                                   a Stored Procedure.                  
 * =======================================================================
 * - Possible Completion Message Ids -                                    
 * ===================================                                    
 * @throws     n/a                                                        
 * =======================================================================
 */
@Service
public class IsCurrentProcessStoredProcedureService extends BaseProcessEntryMethodsService {

    public boolean execute(IsCurrentProcessStoredProcedureContext context) {
        // TODO: Implementation is missing

        return false;
    }
}
