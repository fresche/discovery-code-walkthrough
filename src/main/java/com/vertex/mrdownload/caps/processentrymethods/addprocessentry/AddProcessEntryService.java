package com.vertex.mrdownload.caps.processentrymethods.addprocessentry;

import com.google.common.collect.ImmutableMap;
import com.vertex.mrdownload.ProgramStatusDataStructure;
import com.vertex.mrdownload.caps.exceptionentrymethods.getbillselectexceptiontaskid.GetBillSelectExceptionTaskIdContext;
import com.vertex.mrdownload.caps.exceptionentrymethods.getbillselectexceptiontaskid.GetBillSelectExceptionTaskIdService;
import com.vertex.mrdownload.caps.exceptionentrymethods.getbudgetselectexceptiontaskid.GetBudgetSelectExceptionTaskIdContext;
import com.vertex.mrdownload.caps.exceptionentrymethods.getbudgetselectexceptiontaskid.GetBudgetSelectExceptionTaskIdService;
import com.vertex.mrdownload.caps.packageleveltypedefinitions.ProcessId;
import com.vertex.mrdownload.caps.packageleveltypedefinitions.TaskId;
import com.vertex.mrdownload.caps.processentrymethods.BaseProcessEntryMethodsService;
import com.vertex.mrdownload.caps.processentrymethods.addprocessentryparameter.AddProcessEntryParameterContext;
import com.vertex.mrdownload.caps.processentrymethods.addprocessentryparameter.AddProcessEntryParameterService;
import com.vertex.mrdownload.caps.processentrymethods.setprocessentrybatchheaderbatchdate.SetProcessEntryBatchHeaderBatchDateContext;
import com.vertex.mrdownload.caps.processentrymethods.setprocessentrybatchheaderbatchdate.SetProcessEntryBatchHeaderBatchDateService;
import com.vertex.mrdownload.caps.processrequestmethods.addprocessentrycashrecoveryid.AddProcessEntryCashRecoveryIdContext;
import com.vertex.mrdownload.caps.processrequestmethods.addprocessentrycashrecoveryid.AddProcessEntryCashRecoveryIdService;
import com.vertex.mrdownload.caps.processrequestmethods.getfirstprocessrequestparameter.GetFirstProcessRequestParameterContext;
import com.vertex.mrdownload.caps.processrequestmethods.getfirstprocessrequestparameter.GetFirstProcessRequestParameterService;
import com.vertex.mrdownload.caps.processrequestmethods.getnextprocessrequestparameter.GetNextProcessRequestParameterContext;
import com.vertex.mrdownload.caps.processrequestmethods.getnextprocessrequestparameter.GetNextProcessRequestParameterService;
import com.vertex.mrdownload.caps.processrequestmethods.getprocessrequestparameterdatavalue.GetProcessRequestParameterDataValueContext;
import com.vertex.mrdownload.caps.processrequestmethods.getprocessrequestparameterdatavalue.GetProcessRequestParameterDataValueService;
import com.vertex.mrdownload.caps.taskentrymethods.addtaskentry.AddTaskEntryContext;
import com.vertex.mrdownload.caps.taskentrymethods.addtaskentry.AddTaskEntryService;
import com.vertex.mrdownload.caps.tasktypemethods.getprocesstypetasktype.GetProcessTypeTaskTypeContext;
import com.vertex.mrdownload.caps.tasktypemethods.getprocesstypetasktype.GetProcessTypeTaskTypeService;
import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.BatchId;
import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import com.vertex.mrdownload.global.sqlerrorhandlingmethods.putsqldiagnostics.PutSqlDiagnosticsContext;
import com.vertex.mrdownload.global.sqlerrorhandlingmethods.putsqldiagnostics.PutSqlDiagnosticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.LocalDateTime;

import static com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId.*;
import static java.lang.Integer.parseInt;
import static java.lang.String.format;
import static java.time.ZoneOffset.UTC;
import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.trim;

@Service
public class AddProcessEntryService extends BaseProcessEntryMethodsService {

    private static final String SUBPROCEDURE_NAME = "addProcessEntry";
    private static final String EXCEPTION_CATEGORY = "EXECEPTION";
    private static final String RESEARCH_CATEGORY = "RESEARCH";
    private static final String EXECUTING_STATUS = "EXECUTING";
    private static final int MAXIMUM_UJOBCAL_UPDATE_ATTEMPTS = 3;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private ProgramStatusDataStructure programStatusDataStructure;

    @Autowired
    private GetProcessTypeTaskTypeService getProcessTypeTaskTypeService;

    @Autowired
    private GetProcessRequestParameterDataValueService getProcessRequestParameterDataValueService;

    @Autowired
    private GetBillSelectExceptionTaskIdService getBillSelectExceptionTaskIdService;

    @Autowired
    private GetBudgetSelectExceptionTaskIdService getBudgetSelectExceptionTaskIdService;

    @Autowired
    private GetFirstProcessRequestParameterService getFirstProcessRequestParameterService;

    @Autowired
    private AddTaskEntryService addTaskEntryService;

    @Autowired
    private AddProcessEntryParameterService addProcessEntryParameterService;

    @Autowired
    private GetNextProcessRequestParameterService getNextProcessRequestParameterService;

    @Autowired
    private AddProcessEntryCashRecoveryIdService addProcessEntryCashRecoveryIdService;

    @Autowired
    private SetProcessEntryBatchHeaderBatchDateService setProcessEntryBatchHeaderBatchDateService;

    @Autowired
    private PutSqlDiagnosticsService putSQLDiagnosticsService;

    public void execute(AddProcessEntryContext context) {
        try {
            context.setOutMessageId(new MessageId(""));

            // ======================================================================
            // Transfer input parameter values to local storage for manipulation.
            // ======================================================================
            context.setDsProcessEntry(context.getIoProcessEntryDs());
            context.setProcessType(context.getDsProcessEntry().getPeProcessType());
            context.setProcessInitiationMethod(context.getDsProcessEntry().getPeProcessInitiationMethod());
            context.setRequestTimestamp(context.getDsProcessEntry().getPeRequestTimestamp());
            context.setTaskIdException(new TaskId(0));
            context.setTaskIdResearch(new TaskId(0));
            context.setStatusType(context.getDsProcessEntry().getPeStatusType());
            context.setBatchScope(context.getDsProcessEntry().getPeBatchScope());
            context.setBatchId(context.getDsProcessEntry().getPeBatchId());
            context.setBatchDate(context.getDsProcessEntry().getPeBatchDate());
            context.setCreatedByUser(context.getDsProcessEntry().getPeCreatedByUser());
            context.setInitiationCommand(context.getDsProcessEntry().getPeInitiationCommand());
            context.setProcessRequestId(context.getInProcessRequestId());

            // ======================================================================
            // @assertion:  A valid Process Type must be supplied.
            // ======================================================================
            if (isBlank(context.getProcessType())) {
                context.setOutMessageId(ERROR_INVALID_PROCESS_TYPE);
                return;
            }

            // ======================================================================
            // @assertion:  A valid Process Initiation Method must be supplied.
            // ======================================================================
            if (isBlank(context.getProcessInitiationMethod())) {
                context.setOutMessageId(ERROR_INVALID_PROCESS_INITIATION_METHOD);
                return;
            }

            // ======================================================================
            // @assertion:  A valid Request Timestamp must be supplied.
            // ======================================================================
            if (LocalDateTime.ofEpochSecond(0, 0, UTC).equals(context.getRequestTimestamp())) {
                context.setOutMessageId(ERROR_INVALID_PROCESS_REQUEST_TIMESTAMP);
                return;
            }

            // ======================================================================
            // @assertion:  A valid Process Request Id must be supplied.
            // ======================================================================
            if (context.getProcessId().getValue() <= 0) {
                context.setOutMessageId(ERROR_INVALID_PROCESS_REQUEST_ID);
                return;
            }

            // ======================================================================
            // Retrieve the Exception and Research Item Task Types for the Process
            // Type.
            // ======================================================================
            GetProcessTypeTaskTypeContext getProcessTypeTaskTypeContext = GetProcessTypeTaskTypeContext.builder()
                    .inProcessType(EXCEPTION_CATEGORY)
                    .build();
            context.setTaskTypeException(getProcessTypeTaskTypeService.execute(getProcessTypeTaskTypeContext));
            context.setOutMessageId(getProcessTypeTaskTypeContext.getOutMessageId());

            if (!isBlank(context.getOutMessageId().getValue()))
                return;

            getProcessTypeTaskTypeContext = GetProcessTypeTaskTypeContext.builder()
                    .inProcessType(context.getProcessType())
                    .inCategoryType(RESEARCH_CATEGORY)
                    .build();
            context.setTaskTypeResearch(getProcessTypeTaskTypeService.execute(getProcessTypeTaskTypeContext));
            context.setOutMessageId(getProcessTypeTaskTypeContext.getOutMessageId());

            if (!isBlank(context.getOutMessageId().getValue()))
                return;

            // ======================================================================
            // If the Process Type is a Bill Recalc for a single Bill Select Id,
            // query the Exception Entry Table for any exceptions linked to that Bill
            // Select Id from a prior calculation.  If an exception exists, reuse the
            // Task Id from that exception.
            // ======================================================================
            if ("BILLRECALC".equals(context.getProcessType())) {
                GetProcessRequestParameterDataValueContext getProcessRequestParameterDataValueContext =
                        GetProcessRequestParameterDataValueContext.builder()
                                .inProcessRequestId(context.getProcessRequestId())
                                .inDataLabel("BILL_SELECT_ID")
                                .build();
                context.setParameterDataValue(
                        getProcessRequestParameterDataValueService.execute(getProcessRequestParameterDataValueContext));
                context.setOutMessageId(getProcessRequestParameterDataValueContext.getOutMessageId());

                if (!isBlank(context.getOutMessageId().getValue()))
                    return;

                if (!isBlank(context.getParameterDataValue()))
                    context.setBillSelectId(parseInt(trim(context.getParameterDataValue())));

                if (context.getBillSelectId() != 0) {
                    GetBillSelectExceptionTaskIdContext getBillSelectExceptionTaskIdContext =
                            GetBillSelectExceptionTaskIdContext.builder()
                                    .inBillSelectId(context.getBillSelectId())
                                    .build();
                    context.setTaskIdException(
                            getBillSelectExceptionTaskIdService.execute(getBillSelectExceptionTaskIdContext));
                    context.setOutMessageId(getBillSelectExceptionTaskIdContext.getOutMessageId());

                    if (!isBlank(context.getOutMessageId().getValue()))
                        return;
                }
            }

            // ======================================================================
            // If the Process Type is a Budget Recalc for a single Budget Select Id,
            // query the Exception Entry Table for any exceptions linked to that Bill
            // Select Id from a prior calculation.  If an exception exists, reuse the
            // Task Id from that exception.
            // ======================================================================
            if ("BDGREVLDT".equals(context.getProcessType())) {
                GetProcessRequestParameterDataValueContext getProcessRequestParameterDataValueContext =
                        GetProcessRequestParameterDataValueContext.builder()
                                .inProcessRequestId(context.getProcessRequestId())
                                .inDataLabel("ACCOUNT_ID")
                                .build();
                context.setParameterDataValue(
                        getProcessRequestParameterDataValueService.execute(getProcessRequestParameterDataValueContext));
                context.setOutMessageId(getProcessRequestParameterDataValueContext.getOutMessageId());

                if (!isBlank(context.getOutMessageId().getValue()))
                    return;

                if (!isBlank(context.getParameterDataValue()))
                    context.setAccountId(parseInt(trim(context.getParameterDataValue())));

                getProcessRequestParameterDataValueContext =
                        GetProcessRequestParameterDataValueContext.builder()
                                .inProcessRequestId(context.getProcessRequestId())
                                .inDataLabel("PREMISES_ID")
                                .build();
                context.setParameterDataValue(
                        getProcessRequestParameterDataValueService.execute(getProcessRequestParameterDataValueContext));
                context.setOutMessageId(getProcessRequestParameterDataValueContext.getOutMessageId());

                if (!isBlank(context.getOutMessageId().getValue()))
                    return;

                if (!isBlank(context.getParameterDataValue()))
                    context.setPremisesId(format("%1$-15s", trim(context.getParameterDataValue())));

                if (context.getAccountId() != 0) {
                    GetBudgetSelectExceptionTaskIdContext getBudgetSelectExceptionTaskIdContext =
                            GetBudgetSelectExceptionTaskIdContext.builder()
                                    .inAccountId(context.getBillSelectId())
                                    .inPremisesId(context.getPremisesId())
                                    .build();
                    context.setTaskIdException(
                            getBudgetSelectExceptionTaskIdService.execute(getBudgetSelectExceptionTaskIdContext));
                    context.setOutMessageId(getBudgetSelectExceptionTaskIdContext.getOutMessageId());

                    if (!isBlank(context.getOutMessageId().getValue()))
                        return;
                }
            }

            // ======================================================================
            // If an existing 'Exception' Task entry is being reused, reuse the
            // 'Research Item' Task entry also.
            // ======================================================================
            if (context.getTaskIdException() != TaskId.ZERO) {
                try {
                    context.setTaskIdResearch(new TaskId(
                            ofNullable(jdbcTemplate.query(
                                    new StringBuilder()
                                            .append("select ifnull( max(RESEARCH_TASK_ID), 0) ")
                                            .append("from   UPRCENT                           ")
                                            .append("where  TASK_ID = ?                       ")
                                            .toString(),
                                    (ps) -> ps.setInt(1, context.getTaskIdException().getValue()),
                                    (ResultSet rs) -> rs.getInt(0))).orElse(0)));
                    context.setSqlCode(SQL_SUCCESSFUL);
                } catch (EmptyResultDataAccessException e) {
                    context.setSqlCode(SQL_ROW_NOT_FOUND);
                } catch (DataAccessException e) {
                    context.setSqlCode(SQL_SYSTEM_ERROR);
                }

                //    If no matching rows are found, the MAX function will return null.
                //    Therefore, an SQLCODE value of +100 (Row Not Found) would logically
                //    never occur.
                if (context.getSqlCode() == SQL_SUCCESSFUL) {
                    PutSqlDiagnosticsContext putSqlDiagnosticsContext =
                            PutSqlDiagnosticsContext.builder()
                                    .inProgramName(programStatusDataStructure.getProgramName())
                                    .inSubprocedureName(format("%s 1", SUBPROCEDURE_NAME))
                                    .inHostVariables(format("processId=%s, taskIdException=%s",
                                            context.getProcessId(), context.getTaskIdException()))
                                    .build();
                    putSQLDiagnosticsService.execute(putSqlDiagnosticsContext);
                    context.setOutMessageId(ERROR_DATABASE_ACCESS_ERROR);
                    return;
                }
            }

            // ======================================================================
            // Create the 'Exception' and 'Research Item' Task Types with the
            // resulting Task Ids being returned.
            // ======================================================================
            if (context.getTaskIdException().getValue() == 0) {
                AddTaskEntryContext addTaskEntryContext = AddTaskEntryContext.builder()
                        .inTaskType(context.getTaskTypeException())
                        .build();
                addTaskEntryService.execute(addTaskEntryContext);
                context.setTaskIdException(addTaskEntryContext.getOutTaskId());
                context.setOutMessageId(addTaskEntryContext.getOutMessageId());

                if (!isBlank(context.getOutMessageId().getValue()))
                    return;
            }

            if (context.getTaskIdResearch().getValue() == 0) {
                AddTaskEntryContext addTaskEntryContext = AddTaskEntryContext.builder()
                        .inTaskType(context.getTaskTypeResearch())
                        .build();
                addTaskEntryService.execute(addTaskEntryContext);
                context.setTaskIdResearch(addTaskEntryContext.getOutTaskId());
                context.setOutMessageId(addTaskEntryContext.getOutMessageId());

                if (!isBlank(context.getOutMessageId().getValue()))
                    return;
            }

            // ======================================================================
            // Status Type will be set to pre-defined value for Process Entry.
            // ======================================================================
            context.setStatusType(EXECUTING_STATUS);

            // ======================================================================
            // Add an entry to the Process Entry Table.
            // ======================================================================
            try {
                // retrieve the generated surrogate key value
                context.setProcessId(new ProcessId(
                        new SimpleJdbcInsert(jdbcTemplate)
                                .withTableName("UPRCENT")
                                .executeAndReturnKey(ImmutableMap.<String, Object>builder()
                                        .put("PROCESS_TYPE", context.getProcessType())
                                        .put("TASK_ID", context.getTaskIdException())
                                        .put("STATUS_TYPE", context.getStatusType())
                                        .put("JOB_NAME", programStatusDataStructure.getProgramName())
                                        .put("JOB_NUMBER", programStatusDataStructure.getJobNumber())
                                        .put("JOB_USER_ID", programStatusDataStructure.getJobUser())
                                        .put("PROCESS_INITIATION_METHOD", context.getProcessInitiationMethod())
                                        .put("BATCH_SCOPE", context.getBatchScope())
                                        .put("BATCH_ID", ofNullable(context.getBatchId()).orElse(new BatchId("")))
                                        .put("BATCH_DATE",
                                                ofNullable(context.getBatchDate()).orElse(LocalDate.of(1, 1, 1)))
                                        .put("REQUEST_TIMESTAMP", context.getRequestTimestamp())
                                        .put("INITIATION_COMMAND",
                                                ofNullable(context.getInitiationCommand()).orElse(""))
                                        .put("RESEARCH_TASK_ID", context.getTaskIdResearch())
                                        .put("CREATED_BY_USER", context.getCreatedByUser())
                                        .build())
                                .intValue()));
                context.setSqlCode(SQL_SUCCESSFUL);
            } catch (EmptyResultDataAccessException e) {
                context.setSqlCode(SQL_ROW_NOT_FOUND);
            } catch (DataAccessException e) {
                context.setSqlCode(SQL_SYSTEM_ERROR);
            }

            // Error-Handling
            if (context.getSqlCode() == SQL_SUCCESSFUL) {
                // TODO: Unsupported Operation -> %addr(SQLCA)
                PutSqlDiagnosticsContext putSqlDiagnosticsContext =
                        PutSqlDiagnosticsContext.builder()
                                .inProgramName(programStatusDataStructure.getProgramName())
                                .inSubprocedureName(format("%s 1", SUBPROCEDURE_NAME))
                                .inHostVariables(format("processType=%s, taskIdException=%s, taskIdResearch=%s, statusType=%s, " +
                                                "processInitiationMethod=%s, batchId=%s, batchDate=%s, requestTimestamp=%s, " +
                                                "initiationCommand=%s",
                                        trim(context.getProcessType()),
                                        String.valueOf(context.getTaskIdException()),
                                        String.valueOf(context.getTaskIdResearch()),
                                        trim(context.getStatusType()),
                                        trim(context.getProcessInitiationMethod()),
                                        trim(context.getBatchId().getValue()),
                                        context.getBatchDate(),
                                        context.getRequestTimestamp(),
                                        context.getInitiationCommand()))
                                .build();
                putSQLDiagnosticsService.execute(putSqlDiagnosticsContext);
                context.setOutMessageId(ERROR_DATABASE_ACCESS_ERROR);
                return;
            }

            // ======================================================================
            // Add Process Entry Parameter entries for each Process Request Parameter.
            // ======================================================================
            GetFirstProcessRequestParameterContext getFirstProcessRequestParameterContext =
                    GetFirstProcessRequestParameterContext.builder()
                            .inProcessRequestId(context.getProcessRequestId())
                            .build();
            context.setDsProcessRequestParameter(
                    getFirstProcessRequestParameterService.execute(getFirstProcessRequestParameterContext));
            context.setOutMessageId(getFirstProcessRequestParameterContext.getOutMessageId());

            if (context.getOutMessageId() == BLANK &&
                    context.getOutMessageId() == WARNING_ALL_MATCHING_ROWS_HAVE_BEEN_RETURNED) {
                return;
            }

            while (context.getOutMessageId() == BLANK) {
                context.getDsProcessEntryParameter().setPepProcessId(context.getProcessId());
                context.getDsProcessEntryParameter().setPepDataLabel(
                        context.getDsProcessRequestParameter().getPrpDataLabel());
                context.getDsProcessEntryParameter().setPepSequence(
                        context.getDsProcessRequestParameter().getPrpSequence());
                context.getDsProcessEntryParameter().setPepDataValue(
                        context.getDsProcessRequestParameter().getPrpDataValue());

                AddProcessEntryParameterContext addProcessEntryParameterContext =
                        AddProcessEntryParameterContext.builder()
                                .inProcessEntryParameterDs(context.getDsProcessEntryParameter())
                                .build();
                addProcessEntryParameterService.execute(addProcessEntryParameterContext);
                context.setOutMessageId(addProcessEntryParameterContext.getOutMessageId());

                if (context.getOutMessageId() == BLANK)
                    return;

                GetNextProcessRequestParameterContext getNextProcessRequestParameterContext =
                        GetNextProcessRequestParameterContext.builder()
                                .build();
                context.setDsProcessRequestParameter(
                        getNextProcessRequestParameterService.execute(getNextProcessRequestParameterContext));
                context.setOutMessageId(getNextProcessRequestParameterContext.getOutMessageId());

                if (context.getOutMessageId() != BLANK &&
                        context.getOutMessageId() != WARNING_ALL_MATCHING_ROWS_HAVE_BEEN_RETURNED)
                    return;
            }

            // ======================================================================
            // Add Process Entry Cash Recovery Id for the Process Request
            // ======================================================================
            AddProcessEntryCashRecoveryIdContext addProcessEntryCashRecoveryIdContext =
                    AddProcessEntryCashRecoveryIdContext.builder()
                            .inProcessRequestId(context.getProcessRequestId())
                            .inProcessId(context.getProcessId())
                            .build();
            addProcessEntryCashRecoveryIdService.execute(addProcessEntryCashRecoveryIdContext);
            context.setCashRecoveryId(addProcessEntryCashRecoveryIdContext.getOutCashRecoveryId());
            context.setOutMessageId(addProcessEntryCashRecoveryIdContext.getOutMessageId());

            if (context.getOutMessageId() != BLANK)
                return;

            context.setUpdateCount(0);

            do {
                context.setUpdateCount(context.getUpdateCount() + 1);

                //    ===================================================================
                //    Update the Job Calendar entry with the id of the Process Entry and
                //    remove the reference to the Process Request which will soon be
                //    deleted.
                //    ===================================================================
                try {
                    jdbcTemplate.update(
                            new StringBuilder()
                                    .append("update UJOBCAL             ")
                                    .append("set    STATUS_CODE = ?,    ")
                                    .append("       REQUEST_ID  = null, ")
                                    .append("       PROCESS_ID  = ?     ")
                                    .append("where  REQUEST_ID  = ?     ")
                                    .toString(),
                            (ps) -> {
                                ps.setString(1, context.getStatusType());
                                ps.setInt(2, context.getProcessId().getValue());
                                ps.setInt(3, context.getProcessRequestId().getValue());
                            });
                } catch (EmptyResultDataAccessException e) {
                    context.setSqlCode(SQL_ROW_NOT_FOUND);
                } catch (DataAccessException e) {
                    context.setSqlCode(SQL_SYSTEM_ERROR);
                }

                if (context.getSqlCode() == SQL_SUCCESSFUL || context.getSqlCode() == SQL_ROW_NOT_FOUND) {
                    // not submitted via Job Scheduler drop through.  loop will end since loop condition has been met.
                } else if (context.getSqlCode() == SQL_TABLE_ROW_IN_USE &&
                        context.getUpdateCount() < MAXIMUM_UJOBCAL_UPDATE_ATTEMPTS) {
                    wait();
                } else {
                    // TODO: Unsupported Instruction -> %addr(SQLCA)
                    PutSqlDiagnosticsContext putSqlDiagnosticsContext =
                            PutSqlDiagnosticsContext.builder()
                                    .inProgramName(programStatusDataStructure.getProgramName())
                                    .inSubprocedureName(SUBPROCEDURE_NAME)
                                    .inHostVariables(format("processRequestId=%d, processId=%d, statusType=%s",
                                            context.getProcessRequestId().getValue(),
                                            context.getProcessId().getValue(),
                                            trim(context.getStatusType())))
                                    .build();
                    putSQLDiagnosticsService.execute(putSqlDiagnosticsContext);
                    context.setOutMessageId(ERROR_DATABASE_ACCESS_ERROR);
                    return;
                }
            } while (context.getSqlCode() != SQL_SUCCESSFUL ||
                    context.getSqlCode() != SQL_ROW_NOT_FOUND ||
                    context.getUpdateCount() > MAXIMUM_UJOBCAL_UPDATE_ATTEMPTS);

            // Store the Process Id into the data structure
            context.getIoProcessEntryDs().setPeProcessId(context.getProcessId());
            context.getIoProcessEntryDs().setPeTaskId(context.getTaskIdException());

            // ======================================================================
            // If a Batch Date was specified on the Process Request, ensure that the
            // Batch "header" contains a date also.
            // ======================================================================
            if (context.getBatchId() != BatchId.BLANK && context.getBatchDate().isEqual(LocalDate.of(1, 1, 1))) {
                SetProcessEntryBatchHeaderBatchDateContext setProcessEntryBatchHeaderBatchDateContext =
                        SetProcessEntryBatchHeaderBatchDateContext.builder()
                                .inProcessType(context.getProcessType())
                                .inBatchId(context.getBatchId())
                                .inBatchDate(context.getBatchDate())
                                .inCashRecoveryId(context.getCashRecoveryId())
                                .build();
                setProcessEntryBatchHeaderBatchDateService.execute(setProcessEntryBatchHeaderBatchDateContext);
                context.setOutMessageId(setProcessEntryBatchHeaderBatchDateContext.getOutMessageId());

                if (context.getOutMessageId() != BLANK)
                    return;
            }
        } catch (Exception e) {
            context.setOutMessageId(new MessageId(format("%s%s",
                    programStatusDataStructure.getExceptionType(),
                    programStatusDataStructure.getExceptionNumber())));
            putProgramDiagnostics(
                    programStatusDataStructure.getProgramName(),
                    format("%s%s",
                            programStatusDataStructure.getExceptionType(),
                            programStatusDataStructure.getExceptionNumber()),
                    programStatusDataStructure.getRetrievedExceptionData(),
                    format("%s 0", SUBPROCEDURE_NAME));
            // TODO: Unsupported instruction -> dump(A)
        }
    }

    private void putProgramDiagnostics(
            String programName,
            String format,
            String retrievedExceptionData,
            String format1) {

    }
}
