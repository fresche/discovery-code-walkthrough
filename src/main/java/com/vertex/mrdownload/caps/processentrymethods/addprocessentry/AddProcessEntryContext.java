package com.vertex.mrdownload.caps.processentrymethods.addprocessentry;

import com.vertex.mrdownload.caps.packageleveltypedefinitions.ProcessId;
import com.vertex.mrdownload.caps.packageleveltypedefinitions.ProcessRequestId;
import com.vertex.mrdownload.caps.packageleveltypedefinitions.TaskId;
import com.vertex.mrdownload.caps.processentrymethods.ProcessEntryAttributes;
import com.vertex.mrdownload.caps.processentrymethods.ProcessEntryParameterAttributes;
import com.vertex.mrdownload.caps.processentrymethods.ProcessRequestParameterAttributes;
import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.BatchId;
import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Builder
public class AddProcessEntryContext {

    // Parameters
    private ProcessEntryAttributes ioProcessEntryDs;
    private ProcessRequestId inProcessRequestId;
    private MessageId outMessageId;

    // Database Access
    private int sqlCode;

    // Local Variables
    private ProcessEntryAttributes dsProcessEntry;
    private ProcessEntryParameterAttributes dsProcessEntryParameter;
    private ProcessRequestParameterAttributes dsProcessRequestParameter;
    private int accountId;
    private LocalDate batchDate;
    private BatchId batchId;
    private String batchScope;
    private int billSelectId;
    private String cashRecoveryId;
    private String createdByUser;
    private String initiationCommand;
    private String parameterDataValue;
    private String premisesId;
    private ProcessId processId;
    private String processInitiationMethod;
    private ProcessRequestId processRequestId;
    private String processType;
    private LocalDateTime requestTimestamp;
    private String statusType;
    private TaskId taskIdException;
    private TaskId taskIdResearch;
    private String taskTypeException;
    private String taskTypeResearch;
    private int updateCount;
}
