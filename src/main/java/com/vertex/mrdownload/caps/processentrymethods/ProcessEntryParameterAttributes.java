package com.vertex.mrdownload.caps.processentrymethods;

import com.vertex.mrdownload.caps.packageleveltypedefinitions.ProcessId;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class ProcessEntryParameterAttributes {

    private String pepProcessEntryParameterId;
    private ProcessId pepProcessId;
    private String pepDataLabel;
    private int pepSequence;
    private String pepDataValue;
}
