package com.vertex.mrdownload.caps.processentrymethods.iscurrentprocesscritical;

import com.vertex.mrdownload.caps.processentrymethods.BaseProcessEntryMethodsService;
import org.springframework.stereotype.Service;

/**
 * =======================================================================
 * @subprocedure isCurrentProcessEntryCritical                            
 * @module       APSPRCENT4                                               
 * @author       David Swartzendruber (DAVIDL)                            
 * @dateWritten  02/11/2015                                               
 * =======================================================================
 * @purpose    This method will return a Boolean value indicating whether 
 *             the process currently executing is a critial process.      
 * =======================================================================
 * - Constraints / Assertions -                                           
 * ============================                                           
 * @assertion  A critical process is defined on the process type.         
 *             The critical process flag will be '1' to signify           
 *             a critical process.                                        
 * @assertion  This subprocedure does not return the standard outMessageId
 *             parameter.  Instead, if an error were to occur during      
 *             execution of this method, a value of *ON will be returned. 
 * =======================================================================
 * - Parameters -                                                         
 * ==============                                                         
 * @return     <BOOLEAN>             This Boolean value is used to        
 *                                   indicate whether the process         
 *                                   currently executing is a critical    
 *                                   process.                             
 * =======================================================================
 * - Possible Completion Message Ids -                                    
 * ===================================                                    
 * @throws     n/a                                                        
 * =======================================================================
 */
@Service
public class IsCurrentProcessEntryCriticalService extends BaseProcessEntryMethodsService {

    public boolean execute(IsCurrentProcessEntryCriticalContext context) {
        // TODO: Implementation is missing

        return false;
    }
}
