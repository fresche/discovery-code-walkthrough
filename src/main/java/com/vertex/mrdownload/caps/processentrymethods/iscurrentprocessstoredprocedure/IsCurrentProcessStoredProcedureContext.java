package com.vertex.mrdownload.caps.processentrymethods.iscurrentprocessstoredprocedure;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class IsCurrentProcessStoredProcedureContext {

}
