package com.vertex.mrdownload.caps.processentrymethods;

import com.vertex.mrdownload.caps.packageleveltypedefinitions.ProcessId;
import com.vertex.mrdownload.caps.packageleveltypedefinitions.TaskId;
import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.BatchId;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Builder
public class ProcessEntryAttributes {

    private ProcessId peProcessId;
    private String peProcessType;
    private TaskId peTaskId;
    private String peStatusType;
    private String peJobName;
    private int peJobNumber;
    private String peJobUserId;
    private String peProcessInitiationMethod;
    private String peBatchScope;
    private BatchId peBatchId;
    private LocalDate peBatchDate;
    private LocalDateTime peRequestTimestamp;
    private LocalDateTime peStartTimestamp;
    private LocalDateTime peCompletionTimestamp;
    private String peCreatedByUser;
    private String peInitiationCommand;
}
