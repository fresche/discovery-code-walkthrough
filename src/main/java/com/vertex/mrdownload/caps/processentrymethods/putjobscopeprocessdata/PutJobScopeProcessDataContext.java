package com.vertex.mrdownload.caps.processentrymethods.putjobscopeprocessdata;

import com.vertex.mrdownload.caps.packageleveltypedefinitions.ProcessId;
import com.vertex.mrdownload.caps.packageleveltypedefinitions.TaskId;
import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PutJobScopeProcessDataContext {

    // Parameters
    private ProcessId inProcessId;
    private TaskId inTaskId;
    private MessageId outMessageId;
}
