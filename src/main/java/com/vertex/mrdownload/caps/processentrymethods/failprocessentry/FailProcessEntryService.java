package com.vertex.mrdownload.caps.processentrymethods.failprocessentry;

import com.vertex.mrdownload.caps.processentrymethods.BaseProcessEntryMethodsService;
import org.springframework.stereotype.Service;

/**
 * =======================================================================
 * @subprocedure failProcessEntry
 * @module       APSPRCENT1
 * =======================================================================
 * @purpose    This method updates the current Process Entry (UPRCENT)
 *             with a "FAILED" status.
 * =======================================================================
 * - Constraints / Assertions -
 * ============================
 * @assertion  The Process Id will be obtained from QTEMP/PROCESSID.
 * @assertion  Since this method should only be invoked when doing error
 *             handling for a fatal error condition, an SQL Isolation
 *             Level of No Commit (NC) is used to ensure that the database
 *             updates are immediate and permanent.
 * @assertion  Since this method should only be invoked when doing error
 *             handling for a fatal error condition, standard parameter
 *             outMessageId is not used since the process has already
 *             failed.  Consequently, any message id from this method
 *             would only obscure the primary error condition.
 * =======================================================================
 * - Parameters -
 * ==============
 * @return     n/a
 * @param      none
 * =======================================================================
 * - Possible Completion Message Ids -
 * ===================================
 * @throws     n/a
 * =======================================================================
 */
@Service
public class FailProcessEntryService extends BaseProcessEntryMethodsService {

    public void execute(FailProcessEntryContext context) {
        // TODO: Implementation is missing
    }
}
