package com.vertex.mrdownload.caps.processentrymethods;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProcessRequestParameterAttributes {

    private String prpProcessRequestParameterId;
    private String prpDataLabel;
    private int prpSequence;
    private String prpDataValue;
}
