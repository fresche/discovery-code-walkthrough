package com.vertex.mrdownload.caps.processentrymethods.completeprocessentry;

import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.UIBackOfficeProcessDriverMeterReadDownloadService;
import org.springframework.stereotype.Service;

/**
 * =======================================================================
 * @subprocedure executeProcess
 * =======================================================================
 * @purpose Execute Meter Read Download
 * =======================================================================
 */
@Service
public class CompleteProcessEntryService extends UIBackOfficeProcessDriverMeterReadDownloadService {

    public String execute(CompleteProcessEntryContext context) {
        // TODO: Implementation is missing

        return "";
    }
}
