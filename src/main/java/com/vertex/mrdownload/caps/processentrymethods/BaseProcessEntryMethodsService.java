package com.vertex.mrdownload.caps.processentrymethods;

import com.vertex.mrdownload.BaseService;

import static java.lang.String.format;

public abstract class BaseProcessEntryMethodsService extends BaseService {

    protected static final String AMPERSAND = "&";
    protected static final String APOSTROPHE = "''";
    protected static final String ASTERISK = "*";
    protected static final String AT_SIGN = "@";
    protected static final String AXON = "@";
    protected static final String BACKWARD_SLASH = "\\";
    protected static final String CARRIAGE_RETURN = "\n";
    protected static final String CLOSE_PARENTHESIS = ")";
    protected static final String COLON = ":";
    protected static final String COMMA = ",";
    protected static final String DOLLAR = "$";
    protected static final String EQUAL_SIGN = "=";
    protected static final String EXCLAMATION_MARK = "!";
    protected static final String FORWARD_SLASH = "/";
    protected static final String GREATER_THAN = ">";
    protected static final String HYPHEN = "-";
    protected static final String LESS_THAN = "<";
    protected static final String MINUS_SIGN = "-";
    protected static final String NUMBER_SIGN = "#";
    protected static final String OPEN_PARENTHESIS = "(";
    protected static final String PERCENT_SIGN = "%";
    protected static final String PERIOD = ".";
    protected static final String PLUS_SIGN = "+";
    protected static final String QUESTION_MARK = "?";
    protected static final String QUOTATION_MARK = "\"";
    protected static final String SEMICOLON = ";";
    protected static final String SINGLE_BLANK = " ";
    protected static final String TAB = "\t";
    protected static final String TILDE = "~";
    protected static final String UNDERSCORE = "_";
    protected static final String CHEVRON_LEFT = "\u008A";
    protected static final String CHEVRON_RIGHT = "\u008B";

    protected static final String ERROR_INVALID_TASK_ID = "APS0193";
    protected static final String ERROR_INVALID_TASK_TYPE = "APS0212";
    protected static final String ERROR_NO_TASK_TYPE_ASSOCIATED_WITH_PROCESS_TYPE = "APS0227";

    protected static final int SQL_SUCCESSFUL = 0;
    protected static final int SQL_END_OF_CURSOR = 100;
    protected static final int SQL_ROW_NOT_FOUND = 100;
    protected static final int SQL_OBJECT_NOT_FOUND = -204;
    protected static final int SQL_INDICATOR_VARIABLE_REQUIRED = -305;
    protected static final int SQL_CURSOR_NOT_OPEN = -501;
    protected static final int SQL_CURSOR_ALREADY_OPEN = -502;
    protected static final int SQL_REFERENTIAL_INTEGRITY_INSERT_ERROR = -530;
    protected static final int SQL_REFERENTIAL_INTEGRITY_UPDATE_ERROR = -531;
    protected static final int SQL_REFERENTIAL_INTEGRITY_DELETE_ERROR = -532;
    protected static final int SQL_CHECK_CONSTRAINT_VIOLATION = -545;
    protected static final int SQL_TABLE_ALREADY_EXISTS = -601;
    protected static final int SQL_DUPLICATE_KEY = -803;
    protected static final int SQL_MULTIPLE_ROWS_RETURNED = -811;
    protected static final int SQL_SYSTEM_ERROR = -901;
    protected static final int SQL_DATA_CHANGE_VIOLATION = -907;
    protected static final int SQL_TABLE_ROW_IN_USE = -913;

    public BaseProcessEntryMethodsService() {
        super("APSPRCENT1");
    }
}
