package com.vertex.mrdownload.caps.processentrymethods.setcurrentprocesscompletionmessageid;

import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SetCurrentProcessCompletionMessageIdContext {

    // Parameters
    private MessageId inMessageId;
    private MessageId outMessageId;
}
