package com.vertex.mrdownload.caps.processentrymethods.loadprocessentrydatastructure;

import com.vertex.mrdownload.caps.processentrymethods.ProcessEntryAttributes;
import com.vertex.mrdownload.caps.processrequestmethods.ProcessRequestAttributes;
import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LoadProcessEntryDataStructureContext {

    // Parameters
    private ProcessRequestAttributes inProcessRequestDs;
    private ProcessEntryAttributes outProcessEntryDs;
    private MessageId outMessageId;
}
