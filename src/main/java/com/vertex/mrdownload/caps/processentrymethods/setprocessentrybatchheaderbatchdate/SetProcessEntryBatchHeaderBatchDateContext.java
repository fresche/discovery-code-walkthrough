package com.vertex.mrdownload.caps.processentrymethods.setprocessentrybatchheaderbatchdate;

import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.BatchId;
import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
public class SetProcessEntryBatchHeaderBatchDateContext {

    // Parameters
    private String inProcessType;
    private BatchId inBatchId;
    private LocalDate inBatchDate;
    private String inCashRecoveryId;
    private MessageId outMessageId;
}
