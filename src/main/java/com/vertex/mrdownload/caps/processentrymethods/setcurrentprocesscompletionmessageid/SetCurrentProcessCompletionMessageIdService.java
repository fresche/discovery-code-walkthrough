package com.vertex.mrdownload.caps.processentrymethods.setcurrentprocesscompletionmessageid;

import com.vertex.mrdownload.caps.processentrymethods.BaseProcessEntryMethodsService;
import org.springframework.stereotype.Service;

/**
 * =======================================================================
 * @subprocedure setCurrentProcessCompletionMessageId
 * @module       APSPRCENT1
 * @author       Jeff Haddix (OCJEF01)
 * @dateWritten  11/04/2011
 * =======================================================================
 * @purpose    This method will set the Completion Message Id for the
 *             process currently executing.
 * =======================================================================
 * - Constraints / Assertions -
 * ============================
 * @assertion  This method is only relevant when the current process was
 *             initiated via a Stored Procedure call.
 * @assertion  The Completion Message Id is stored in QTEMP/PROCESSID.
 * =======================================================================
 * - Parameters -
 * ==============
 * @return     n/a
 * @param      inMessageId           This is the Completion Message Id
 *                                   value to be stored into for the
 *                                   process currently executing.
 * @param      outMessageId          In the event that an error is
 *                                   encountered during execution of this
 *                                   procedure, this parameter will
 *                                   contain the associated message id.
 * =======================================================================
 * - Possible Completion Message Ids -
 * ===================================
 * @throws     GBL0001  Subprocedure interface/processing error was
 *                      encountered.
 * @throws     GBL0002  Database Access Error was encountered.
 * =======================================================================
 */
@Service
public class SetCurrentProcessCompletionMessageIdService extends BaseProcessEntryMethodsService {

    public void execute(SetCurrentProcessCompletionMessageIdContext context) {
        // TODO: Implementation is missing
    }
}
