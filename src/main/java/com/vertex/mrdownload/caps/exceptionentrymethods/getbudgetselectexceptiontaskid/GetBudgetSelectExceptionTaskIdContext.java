package com.vertex.mrdownload.caps.exceptionentrymethods.getbudgetselectexceptiontaskid;

import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GetBudgetSelectExceptionTaskIdContext {

    private int inAccountId;
    private String inPremisesId;
    private MessageId outMessageId;
}
