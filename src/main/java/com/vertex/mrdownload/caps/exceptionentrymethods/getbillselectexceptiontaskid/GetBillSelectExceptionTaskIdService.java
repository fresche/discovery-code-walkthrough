package com.vertex.mrdownload.caps.exceptionentrymethods.getbillselectexceptiontaskid;

import com.vertex.mrdownload.caps.exceptionentrymethods.BaseExceptionEntryMethodsService;
import com.vertex.mrdownload.caps.packageleveltypedefinitions.TaskId;
import org.springframework.stereotype.Service;

@Service
public class GetBillSelectExceptionTaskIdService extends BaseExceptionEntryMethodsService {

    public TaskId execute(GetBillSelectExceptionTaskIdContext context) {
        // TODO: Implementation is missing

        return TaskId.ZERO;
    }
}
