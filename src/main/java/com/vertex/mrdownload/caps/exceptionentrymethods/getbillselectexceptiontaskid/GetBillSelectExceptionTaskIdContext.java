package com.vertex.mrdownload.caps.exceptionentrymethods.getbillselectexceptiontaskid;

import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GetBillSelectExceptionTaskIdContext {

    private int inBillSelectId;
    private MessageId outMessageId;
}
