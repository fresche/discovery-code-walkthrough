package com.vertex.mrdownload.caps.exceptionentrymethods.getbudgetselectexceptiontaskid;

import com.vertex.mrdownload.caps.exceptionentrymethods.BaseExceptionEntryMethodsService;
import com.vertex.mrdownload.caps.packageleveltypedefinitions.TaskId;
import org.springframework.stereotype.Service;

@Service
public class GetBudgetSelectExceptionTaskIdService extends BaseExceptionEntryMethodsService {

    public TaskId execute(GetBudgetSelectExceptionTaskIdContext context) {
        // TODO: Implementation is missing

        return TaskId.ZERO;
    }
}
