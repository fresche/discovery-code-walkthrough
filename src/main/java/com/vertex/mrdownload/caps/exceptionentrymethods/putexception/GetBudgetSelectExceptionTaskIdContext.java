package com.vertex.mrdownload.caps.exceptionentrymethods.putexception;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GetBudgetSelectExceptionTaskIdContext {

    private int inAccountId;
    private String inPremisesId;
    private String outMessageId;
}
