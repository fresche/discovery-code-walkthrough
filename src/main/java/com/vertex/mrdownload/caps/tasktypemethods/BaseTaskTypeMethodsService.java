package com.vertex.mrdownload.caps.tasktypemethods;

import com.vertex.mrdownload.BaseService;
import com.vertex.mrdownload.global.programerrorhandlingmethods.putprogramdiagnostics.PutProgramDiagnosticsService;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class BaseTaskTypeMethodsService extends BaseService {

    @Autowired
    protected PutProgramDiagnosticsService putProgramDiagnosticsService;

    public BaseTaskTypeMethodsService() {
        super("APSTSKTYP");
    }
}
