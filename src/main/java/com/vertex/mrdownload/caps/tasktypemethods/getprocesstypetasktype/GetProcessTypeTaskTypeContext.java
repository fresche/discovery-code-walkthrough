package com.vertex.mrdownload.caps.tasktypemethods.getprocesstypetasktype;

import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GetProcessTypeTaskTypeContext {

    // Parameters
    private String inProcessType;
    private String inCategoryType;
    private MessageId outMessageId;

    // Database Access
    private int sqlCode;

    // Local Variables
    private String taskType;
}
