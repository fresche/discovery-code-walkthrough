package com.vertex.mrdownload.caps.tasktypemethods.getprocesstypetasktype;

import com.vertex.mrdownload.ProgramStatusDataStructure;
import com.vertex.mrdownload.caps.tasktypemethods.BaseTaskTypeMethodsService;
import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import com.vertex.mrdownload.global.programerrorhandlingmethods.putprogramdiagnostics.PutProgramDiagnosticsContext;
import com.vertex.mrdownload.global.programerrorhandlingmethods.putprogramdiagnostics.PutProgramDiagnosticsService;
import com.vertex.mrdownload.global.sqlerrorhandlingmethods.putsqldiagnostics.PutSqlDiagnosticsContext;
import com.vertex.mrdownload.global.sqlerrorhandlingmethods.putsqldiagnostics.PutSqlDiagnosticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Service;

import static com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId.*;
import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.trim;

@Service
public class GetProcessTypeTaskTypeService extends BaseTaskTypeMethodsService {

    private static final int SQL_SUCCESSFUL = 0;
    private static final int SQL_END_OF_CURSOR = 100;
    private static final int SQL_ROW_NOT_FOUND = 100;
    private static final int SQL_OBJECT_NOT_FOUND = -204;
    private static final int SQL_INDICATOR_VARIABLE_REQUIRED = -305;
    private static final int SQL_CURSOR_NOT_OPEN = -501;
    private static final int SQL_CURSOR_ALREADY_OPEN = -502;
    private static final int SQL_REFERENTIAL_INTEGRITY_INSERT_ERROR = -530;
    private static final int SQL_REFERENTIAL_INTEGRITY_UPDATE_ERROR = -531;
    private static final int SQL_REFERENTIAL_INTEGRITY_DELETE_ERROR = -532;
    private static final int SQL_CHECK_CONSTRAINT_VIOLATION = -545;
    private static final int SQL_TABLE_ALREADY_EXISTS = -601;
    private static final int SQL_DUPLICATE_KEY = -803;
    private static final int SQL_MULTIPLE_ROWS_RETURNED = -811;
    private static final int SQL_SYSTEM_ERROR = -901;
    private static final int SQL_DATA_CHANGE_VIOLATION = -907;
    private static final int SQL_TABLE_ROW_IN_USE = -913;

    private static final String SUBPROCEDURE_NAME = "getProcessTypeTaskType";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private ProgramStatusDataStructure programStatusDataStructure;

    @Autowired
    private PutSqlDiagnosticsService putSqlDiagnosticsService;

    @Autowired
    private PutProgramDiagnosticsService putProgramDiagnosticsService;

    public String execute(GetProcessTypeTaskTypeContext context) {
        try {
            context.setOutMessageId(MessageId.BLANK);
            context.setTaskType("");

            boolean isExistingProcessTypeRv = isExistingProcessType();
            if (isBlank(context.getInProcessType()) || !isExistingProcessTypeRv) {
                context.setOutMessageId(MessageId.ERROR_INVALID_PROCESS_TYPE);
                return "";
            }

            try {
                context.setTaskType(
                        jdbcTemplate.query(
                                new StringBuilder()
                                        .append("select   TASKTYPE                       ")
                                        .append("from     UTSKTYP                        ")
                                        .append("where    PROCESS_TYPE  = :inProcessType ")
                                        .append("and    CATEGORY_TYPE = :inCategoryType  ")
                                        .toString(),
                                (ps) -> {
                                    ps.setString(1, context.getInProcessType());
                                    ps.setString(2, context.getInCategoryType());
                                },
                                (ResultSetExtractor<String>) (rs) -> rs.getString(1)));
                context.setSqlCode(SQL_SUCCESSFUL);
            } catch (EmptyResultDataAccessException e) {
                context.setSqlCode(SQL_ROW_NOT_FOUND);
            } catch (DataAccessException e) {
                context.setSqlCode(SQL_SYSTEM_ERROR);
            }

            if (context.getSqlCode() == SQL_SUCCESSFUL) {
                return context.getTaskType();
            } else if (context.getSqlCode() == SQL_ROW_NOT_FOUND) {
                context.setOutMessageId(ERROR_NO_TASK_TYPE_ASSOCIATED_WITH_PROCESS_TYPE);
                return "";
            } else {
                PutSqlDiagnosticsContext putSqlDiagnosticsContext = PutSqlDiagnosticsContext.builder()
                        .inProgramName(programStatusDataStructure.getProgramName())
                        .inSubprocedureName(SUBPROCEDURE_NAME)
                        .inHostVariables(format("inProcessType=%s", trim(context.getInProcessType())))
                        .inSqlCaPointer("")
                        .build();
                putSqlDiagnosticsService.execute(putSqlDiagnosticsContext);
                context.setOutMessageId(ERROR_DATABASE_ACCESS_ERROR);
                return "";
            }
        } catch (Exception e) {
            PutProgramDiagnosticsContext putProgramDiagnosticsContext = PutProgramDiagnosticsContext.builder()
                    .inProgramName(programStatusDataStructure.getProgramName())
                    .inMessageId(new MessageId(format("%s%s",
                            programStatusDataStructure.getExceptionType(),
                            programStatusDataStructure.getExceptionNumber())))
                    .inMessageText(programStatusDataStructure.getRetrievedExceptionData())
                    .inSubprocedureName(SUBPROCEDURE_NAME)
                    .build();
            putProgramDiagnosticsService.execute(putProgramDiagnosticsContext);
            context.setOutMessageId(ERROR_UNKNOWN_INTERFACE_ERROR);
            // TODO: Unsupported instruction -> dump(A)
            return "";
        }
    }

    private boolean isExistingProcessType() {
        return true;
    }
}
