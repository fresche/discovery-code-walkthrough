package com.vertex.mrdownload.caps.packageleveltypedefinitions;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ProcessId {

    private int value;
}
