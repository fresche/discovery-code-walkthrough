package com.vertex.mrdownload.caps.packageleveltypedefinitions;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class TaskId {

    public static final TaskId ZERO = new TaskId(0);

    private int value;
}
