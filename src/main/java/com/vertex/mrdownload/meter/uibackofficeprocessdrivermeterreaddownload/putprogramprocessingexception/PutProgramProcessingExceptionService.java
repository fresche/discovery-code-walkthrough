package com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.putprogramprocessingexception;

import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.UIBackOfficeProcessDriverMeterReadDownloadService;
import org.springframework.stereotype.Service;

/**
 * =======================================================================
 * @subprocedure putProgramProcessingException                            
 * =======================================================================
 * @purpose    Output a Program Processing Exception (*PROGRAM).          
 * =======================================================================
 * @assertion  With the exception of the Subprocedure Name and the Comment
 *             Text, all variables are globally defined.                  
 * @assertion  Since processing generally can't continue after a Program  
 *             Processing Exception (*PROGRAM), such exceptions are always
 *             fatal, although the scope (transaction versus job) will be 
 *             determined by the routine that detected the exception      
 *             condition.  As such, the exceptionSeverity parameter is    
 *             omitted since the output value would always be equal "F".  
 * =======================================================================
 */
@Service
public class PutProgramProcessingExceptionService extends UIBackOfficeProcessDriverMeterReadDownloadService {

    private static final String SUBPROCEDURE_NAME = "putProgramProcessingException";

    public void execute(PutProgramProcessingExceptionContext context) {
        try {
            //0595.00            putException (EXCEPTION_TYPE_PROGRAM_PROCESSING_ERROR :                                     00/00/00
            //0596.00                          *OMIT :   *OMIT     : *OMIT    : *OMIT              :                         00/00/00
            //0597.00                          'EXCEPTION_PROGRAM'      : sdProgram                :                         00/00/00
            //0598.00                          'EXCEPTION_SUBPROCEDURE' : inSubprocedureName       :                         00/00/00
            //0599.00                          'COMMENT_1'              : inCommentText            :                         00/00/00
            //0600.00                          'BATCH_ID'               : batchId                 );                         00/00/00

            return;
        }
        catch (Exception e) {
            // Error-Handling:  Irrecoverable Exception-Infrastructure Error
            // TODO: Unsupported Instruction -> dump(A)
            //0607.00            endProgramWithError (SUBPROCEDURE_NAME : sdMessageId : sdMessageText);                      00/00/00
        }
    }
}
