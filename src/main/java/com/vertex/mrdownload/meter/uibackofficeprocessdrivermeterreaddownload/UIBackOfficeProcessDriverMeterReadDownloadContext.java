package com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload;

import com.vertex.mrdownload.caps.packageleveltypedefinitions.ProcessId;
import com.vertex.mrdownload.caps.packageleveltypedefinitions.ProcessRequestId;
import com.vertex.mrdownload.caps.packageleveltypedefinitions.TaskId;
import com.vertex.mrdownload.caps.processentrymethods.ProcessEntryAttributes;
import com.vertex.mrdownload.caps.processrequestmethods.ProcessRequestAttributes;
import com.vertex.mrdownload.caps.processtypemethods.ProcessTypeAttributes;
import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.BatchId;
import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.endprogramwitherror.EndProgramWithErrorContext;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class UIBackOfficeProcessDriverMeterReadDownloadContext {

    // Parameters
    private String inProcessRequestIdChar;

    // Local Variables
    private ProcessEntryAttributes dsProcessEntry;
    private ProcessRequestAttributes dsProcessRequest;
    private ProcessTypeAttributes dsProcessType;
    private BatchId batchId;
    private String downloadProgram;
    private ProcessId processId;
    private ProcessRequestId processRequestId;
    private TaskId taskId;
}
