package com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.beginprogram;

import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BeginProgramContext {

    // Local Variables
    private MessageId messageId;
}
