package com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.processrequest;

import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProcessRequestContext {

    // Local Variables
    private MessageId messageId;
}
