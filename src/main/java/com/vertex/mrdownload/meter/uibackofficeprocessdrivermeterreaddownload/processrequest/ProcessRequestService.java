package com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.processrequest;

import com.vertex.mrdownload.caps.processentrymethods.addprocessentry.AddProcessEntryContext;
import com.vertex.mrdownload.caps.processentrymethods.putjobscopeprocessdata.PutJobScopeProcessDataContext;
import com.vertex.mrdownload.global.commitmentcontrolmethods.commitchanges.CommitChangesContext;
import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.UIBackOfficeProcessDriverMeterReadDownloadContext;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.UIBackOfficeProcessDriverMeterReadDownloadService;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.endprogramwitherror.EndProgramWithErrorContext;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.executeprocess.ExecuteProcessContext;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.putprogramprocessingexception.PutProgramProcessingExceptionContext;
import org.springframework.stereotype.Service;

import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * =======================================================================
 * @subprocedure processRequest
 * =======================================================================
 * @purpose  This subprocedure retrieves a process id and a task id, then
 *           execute the meter download.
 * =======================================================================
 */
@Service
public class ProcessRequestService extends UIBackOfficeProcessDriverMeterReadDownloadService {

    private static final String SUBPROCEDURE_NAME = "processRequest";

    public void execute(UIBackOfficeProcessDriverMeterReadDownloadContext parentContext,
                        ProcessRequestContext context) {
        try {
            AddProcessEntryContext addProcessEntryContext = AddProcessEntryContext.builder()
                    .dsProcessEntry(parentContext.getDsProcessEntry())
                    .processRequestId(parentContext.getProcessRequestId())
                    .build();
            addProcessEntryService.execute(addProcessEntryContext);
            context.setMessageId(addProcessEntryContext.getOutMessageId());

            // Error-Handling:  Irrecoverable Process-Scope Error
            if (!isBlank(context.getMessageId().getValue())) {
                // TODO: Unsupported Instruction -> dump(A)
                PutProgramProcessingExceptionContext putProgramProcessingExceptionContext =
                        PutProgramProcessingExceptionContext.putProgramProcessingExceptionContextBuilder()
                                .inSubprocedureName(format("%s 1", SUBPROCEDURE_NAME))
                                .inCommentText("Error: addProcessEntry")
                                .build();
                putProgramProcessingExceptionService.execute(putProgramProcessingExceptionContext);
                EndProgramWithErrorContext endProgramWithErrorContext = EndProgramWithErrorContext.builder()
                        .inSubprocedureName(format("%s 1", SUBPROCEDURE_NAME))
                        .inMessageId(context.getMessageId())
                        .inMessageText("")
                        .build();
                endProgramWithErrorService.execute(parentContext, endProgramWithErrorContext);
            }

            // ======================================================================
            // If the process was to fail after this point, we still want to retain
            // the Process Entry, so commit the Process Entry just created.
            // ======================================================================
            CommitChangesContext commitChangesContext = CommitChangesContext.builder()
                    .build();
            commitChangesService.execute(commitChangesContext);
            context.setMessageId(commitChangesContext.getOutMessageId());

            parentContext.setProcessId(parentContext.getDsProcessEntry().getPeProcessId());
            parentContext.setTaskId(parentContext.getDsProcessEntry().getPeTaskId());

            PutJobScopeProcessDataContext putJobScopeProcessDataContext = PutJobScopeProcessDataContext.builder()
                    .inProcessId(parentContext.getProcessId())
                    .inTaskId(parentContext.getTaskId())
                    .build();
            putJobScopeProcessDataService.execute(putJobScopeProcessDataContext);
            context.setMessageId(putJobScopeProcessDataContext.getOutMessageId());

            // Error-Handling:  Irrecoverable Process-Scope Error
            if (!isBlank(context.getMessageId().getValue())) {
                // TODO: Unsupported Instruction -> dump(A)
                PutProgramProcessingExceptionContext putProgramProcessingExceptionContext =
                        PutProgramProcessingExceptionContext.putProgramProcessingExceptionContextBuilder()
                                .inSubprocedureName(format("%s 2", SUBPROCEDURE_NAME))
                                .inCommentText("putJobScopeProcessData")
                                .build();
                putProgramProcessingExceptionService.execute(putProgramProcessingExceptionContext);
                EndProgramWithErrorContext endProgramWithErrorContext = EndProgramWithErrorContext.builder()
                        .inSubprocedureName(format("%s 2", SUBPROCEDURE_NAME))
                        .inMessageId(context.getMessageId())
                        .inMessageText("")
                        .build();
                endProgramWithErrorService.execute(parentContext, endProgramWithErrorContext);
            }

            ExecuteProcessContext executeProcessContext = ExecuteProcessContext.builder()
                    .build();
            executeProcessService.execute(parentContext, executeProcessContext);

            return;
        }
        catch (Exception e) {
            // Error-Handling:  Irrecoverable Process-Scope Error
            // TODO: Unsupported Instruction -> dump(A)
            PutProgramProcessingExceptionContext putProgramProcessingExceptionContext =
                    PutProgramProcessingExceptionContext.putProgramProcessingExceptionContextBuilder()
                            .inSubprocedureName(format("%s 0", SUBPROCEDURE_NAME))
                            .inCommentText("Program Processing Error")
                            .build();
            putProgramProcessingExceptionService.execute(putProgramProcessingExceptionContext);
            EndProgramWithErrorContext endProgramWithErrorContext = EndProgramWithErrorContext.builder()
                    .inSubprocedureName(SUBPROCEDURE_NAME)
                    .inMessageId(new MessageId(format("%s%s",
                            programStatusDataStructure.getExceptionType(),
                            programStatusDataStructure.getExceptionNumber())))
                    .inMessageText(programStatusDataStructure.getRetrievedExceptionData())
                    .build();
            endProgramWithErrorService.execute(parentContext, endProgramWithErrorContext);
        }
    }
}
