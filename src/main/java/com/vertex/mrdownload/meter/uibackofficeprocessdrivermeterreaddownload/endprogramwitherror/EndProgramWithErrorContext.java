package com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.endprogramwitherror;

import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class EndProgramWithErrorContext {

    // Parameters
    private String inSubprocedureName;
    private MessageId inMessageId;
    private String inMessageText;
}
