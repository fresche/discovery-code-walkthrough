package com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.putprogramprocessingexception;

import com.vertex.mrdownload.caps.packageleveltypedefinitions.ProcessId;
import com.vertex.mrdownload.caps.packageleveltypedefinitions.ProcessRequestId;
import com.vertex.mrdownload.caps.packageleveltypedefinitions.TaskId;
import com.vertex.mrdownload.caps.processentrymethods.ProcessEntryAttributes;
import com.vertex.mrdownload.caps.processrequestmethods.ProcessRequestAttributes;
import com.vertex.mrdownload.caps.processtypemethods.ProcessTypeAttributes;
import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.BatchId;
import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.UIBackOfficeProcessDriverMeterReadDownloadContext;
import lombok.Builder;
import lombok.Data;

@Data
public class PutProgramProcessingExceptionContext extends UIBackOfficeProcessDriverMeterReadDownloadContext {

    // Parameters
    private String inSubprocedureName;
    private String inCommentText;

    // Local Variables
    private MessageId messageId;

    @Builder(builderMethodName = "putProgramProcessingExceptionContextBuilder")
    public PutProgramProcessingExceptionContext(String inProcessRequestIdChar, ProcessEntryAttributes dsProcessEntry,
                                                ProcessRequestAttributes dsProcessRequest,
                                                ProcessTypeAttributes dsProcessType, BatchId batchId,
                                                String downloadProgram, ProcessId processId,
                                                ProcessRequestId processRequestId, TaskId taskId,
                                                String inSubprocedureName, String inCommentText, MessageId messageId) {
        super(inProcessRequestIdChar, dsProcessEntry, dsProcessRequest, dsProcessType, batchId, downloadProgram,
                processId, processRequestId, taskId);
        this.inSubprocedureName = inSubprocedureName;
        this.inCommentText = inCommentText;
        this.messageId = messageId;
    }
}
