package com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.endprogram;

import com.vertex.mrdownload.caps.processentrymethods.completeprocessentry.CompleteProcessEntryContext;
import com.vertex.mrdownload.global.commitmentcontrolmethods.commitchanges.CommitChangesContext;
import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.UIBackOfficeProcessDriverMeterReadDownloadContext;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.UIBackOfficeProcessDriverMeterReadDownloadService;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.endprogramwitherror.EndProgramWithErrorContext;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.putprogramprocessingexception.PutProgramProcessingExceptionContext;
import org.springframework.stereotype.Service;

import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * =======================================================================
 * @subprocedure endProgram
 * =======================================================================
 * @purpose  Terminate Program Processing
 * =======================================================================
 */
@Service
public class EndProgramService extends UIBackOfficeProcessDriverMeterReadDownloadService {

    private static final String SUBPROCEDURE_NAME = "endProgram";

    public void execute(UIBackOfficeProcessDriverMeterReadDownloadContext parentContext, EndProgramContext context) {
        try {
            CompleteProcessEntryContext completeProcessEntryContext = CompleteProcessEntryContext.builder()
                    .inProcessId(parentContext.getProcessId())
                    .build();
            completeProcessEntryService.execute(completeProcessEntryContext);
            context.setMessageId(completeProcessEntryContext.getOutMessageId());

            // Error-Handling:  Recoverable Process-Scope Error
            if (context.getMessageId() != MessageId.BLANK) {
                // TODO: Unsupported Instruction -> dump(A)
                PutProgramProcessingExceptionContext putProgramProcessingExceptionContext =
                        PutProgramProcessingExceptionContext.putProgramProcessingExceptionContextBuilder()
                                .inSubprocedureName(format("%s 1", SUBPROCEDURE_NAME))
                                .inCommentText("Error: completeProcessEntry")
                                .build();
                putProgramProcessingExceptionService.execute(putProgramProcessingExceptionContext);
            }

            CommitChangesContext commitChangesContext = CommitChangesContext.builder()
                    .build();
            commitChangesService.execute(commitChangesContext);

            return;
        }
        catch (Exception e) {
            // Error-Handling:  Irrecoverable Process-Scope Error
            // TODO: Unsupported Instruction -> dump(A)
            PutProgramProcessingExceptionContext putProgramProcessingExceptionContext =
                    PutProgramProcessingExceptionContext.putProgramProcessingExceptionContextBuilder()
                            .inSubprocedureName(format("%s 0", SUBPROCEDURE_NAME))
                            .inCommentText("Program Processing Error")
                            .build();
            putProgramProcessingExceptionService.execute(putProgramProcessingExceptionContext);
            EndProgramWithErrorContext endProgramWithErrorContext = EndProgramWithErrorContext.builder()
                    .inSubprocedureName(SUBPROCEDURE_NAME)
                    .inMessageId(new MessageId(format("%s%s",
                            programStatusDataStructure.getExceptionType(),
                            programStatusDataStructure.getExceptionNumber())))
                    .inMessageText(programStatusDataStructure.getRetrievedExceptionData())
                    .build();
            endProgramWithErrorService.execute(parentContext, endProgramWithErrorContext);
        }
    }
}
