package com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.executeprocess;

import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ExecuteProcessContext {

    // Local Variables
    private MessageId messageId;
}
