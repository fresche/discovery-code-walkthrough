package com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.executemeterreaddownload;

import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.BatchId;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ExecuteMeterReadDownloadContext {

    // Parameters
    private BatchId batchId;
}
