package com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.executeprocess;

import com.vertex.mrdownload.ProgramStatusDataStructure;
import com.vertex.mrdownload.caps.processrequestmethods.deleteprocessrequest.DeleteProcessRequestContext;
import com.vertex.mrdownload.caps.processrequestmethods.deleteprocessrequest.DeleteProcessRequestService;
import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import com.vertex.mrdownload.global.errornotificationmethods.sendemailmessagecustom.SendEmailMessageCustomContext;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.UIBackOfficeProcessDriverMeterReadDownloadContext;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.UIBackOfficeProcessDriverMeterReadDownloadService;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.endprogramwitherror.EndProgramWithErrorContext;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.endprogramwitherror.EndProgramWithErrorService;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.executemeterreaddownload.ExecuteMeterReadDownloadContext;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.putprogramprocessingexception.PutProgramProcessingExceptionContext;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.putprogramprocessingexception.PutProgramProcessingExceptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static java.lang.String.format;

/**
 * =======================================================================
 * @subprocedure executeProcess
 * =======================================================================
 * @purpose Execute Meter Read Download
 * =======================================================================
 */
@Service
public class ExecuteProcessService extends UIBackOfficeProcessDriverMeterReadDownloadService {

    private static final String SUBPROCEDURE_NAME = "executeProcess";

    @Autowired
    private ApplicationContext appCtx;

    @Autowired
    private ProgramStatusDataStructure programStatusDataStructure;

    @Autowired
    private PutProgramProcessingExceptionService putProgramProcessingExceptionService;

    @Autowired
    private EndProgramWithErrorService endProgramWithErrorService;

    @Autowired
    private DeleteProcessRequestService deleteProcessRequestService;

    public void execute(UIBackOfficeProcessDriverMeterReadDownloadContext parentContext,
                        ExecuteProcessContext context) {
        try {
            // ======================================================================
            // The Process Request (UPRCRQS) entry is no longer needed at this point.
            // ----------------------------------------------------------------------
            // Note that a COMMIT is not necessary since deleteProcessRequest uses
            // "WITH NC" when deleting the UPRCRQS entry.
            // ======================================================================
            DeleteProcessRequestContext deleteProcessRequestContext = DeleteProcessRequestContext.builder()
                    .inProcessRequestId(parentContext.getProcessRequestId())
                    .build();
            deleteProcessRequestService.execute(deleteProcessRequestContext);
            context.setMessageId(deleteProcessRequestContext.getOutMessageId());

            // Error-Handling:  Recoverable Process-Scope Error
            if (context.getMessageId() != MessageId.BLANK) {
                // TODO: Unsupported Instruction -> dump(A)
                PutProgramProcessingExceptionContext putProgramProcessingExceptionContext =
                        PutProgramProcessingExceptionContext.putProgramProcessingExceptionContextBuilder()
                                .inSubprocedureName(format("%s 1", SUBPROCEDURE_NAME))
                                .inCommentText("Error: deleteProcessRequest")
                                .build();
                putProgramProcessingExceptionService.execute(putProgramProcessingExceptionContext);
            }

            executeMeterReadDownload(parentContext, context);

            return;
        }
        catch (Exception e) {
            // Error-Handling:  Irrecoverable Process-Scope Error
            // TODO: Unsupported Instruction -> dump(A)
            PutProgramProcessingExceptionContext putProgramProcessingExceptionContext =
                    PutProgramProcessingExceptionContext.putProgramProcessingExceptionContextBuilder()
                            .inSubprocedureName(format("%s 0", SUBPROCEDURE_NAME))
                            .inCommentText("Program Processing Error")
                            .build();
            putProgramProcessingExceptionService.execute(putProgramProcessingExceptionContext);
            EndProgramWithErrorContext endProgramWithErrorContext = EndProgramWithErrorContext.builder()
                    .inSubprocedureName(SUBPROCEDURE_NAME)
                    .inMessageId(new MessageId(format("%s%s",
                            programStatusDataStructure.getExceptionType(),
                            programStatusDataStructure.getExceptionNumber())))
                    .inMessageText(programStatusDataStructure.getRetrievedExceptionData())
                    .build();
            endProgramWithErrorService.execute(parentContext, endProgramWithErrorContext);
        }
    }

    private void executeMeterReadDownload(UIBackOfficeProcessDriverMeterReadDownloadContext parentContext,
                                          ExecuteProcessContext context) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Object executeMeterReadDownload = appCtx.getBean(parentContext.getDownloadProgram());
        Method method = executeMeterReadDownload.getClass()
                .getMethod("execute", ExecuteMeterReadDownloadContext.class);
        ExecuteMeterReadDownloadContext executeMeterReadDownloadContext = ExecuteMeterReadDownloadContext.builder()
                .batchId(parentContext.getBatchId())
                .build();
        method.invoke(executeMeterReadDownload, executeMeterReadDownloadContext);
    }
}
