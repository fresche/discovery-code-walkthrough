package com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.endprogramwitherror;

import com.vertex.mrdownload.ProgramStatusDataStructure;
import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import com.vertex.mrdownload.global.errornotificationmethods.terminatebatchactivationgroup.TerminateBatchActivationGroupContext;
import com.vertex.mrdownload.global.errornotificationmethods.terminatebatchactivationgroup.TerminateBatchActivationGroupService;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.UIBackOfficeProcessDriverMeterReadDownloadContext;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.UIBackOfficeProcessDriverMeterReadDownloadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * =======================================================================
 * @subprocedure endProgramWithError                                      
 * =======================================================================
 * @purpose  Terminate program processing due to an error condition.      
 * =======================================================================
 */
@Service
public class EndProgramWithErrorService extends UIBackOfficeProcessDriverMeterReadDownloadService {

    private static final String SUBPROCEDURE_NAME = "endProgramWithError";
    private static final String FATAL_ERROR_TEXT = "Fatal Error Encountered During Meter Read Download";

    @Autowired
    private ProgramStatusDataStructure programStatusDataStructure;

    @Autowired
    private TerminateBatchActivationGroupService terminateBatchActivationGroupService;

    public void execute(UIBackOfficeProcessDriverMeterReadDownloadContext parentContext,
                        EndProgramWithErrorContext context) {
        try {
            // terminateBatchActivationGroup will do a ROLLBACK
            TerminateBatchActivationGroupContext terminateBatchActivationGroupContext =
                    TerminateBatchActivationGroupContext.builder()
                            .inProgramId(programStatusDataStructure.getProgramName())
                            .inSubprocedureName(context.getInSubprocedureName())
                            .inBatchId(parentContext.getBatchId())
                            .inOverrideEmailMessageId(MessageId.METER_READ_DOWNLOAD_TERMINATING_WITH_ERROR)
                            .inErrorMessageId(context.getInMessageId())
                            .inMessageText(FATAL_ERROR_TEXT)
                            .build();
            terminateBatchActivationGroupService.execute(terminateBatchActivationGroupContext);

            return;
        }
        catch (Exception e) {
            // TODO: Unsupported Instruction -> dump(A)
            TerminateBatchActivationGroupContext terminateBatchActivationGroupContext =
                    TerminateBatchActivationGroupContext.builder()
                            .inProgramId(programStatusDataStructure.getProgramName())
                            .inSubprocedureName(SUBPROCEDURE_NAME)
                            .inBatchId(parentContext.getBatchId())
                            .inOverrideEmailMessageId(MessageId.METER_READ_DOWNLOAD_TERMINATING_WITH_ERROR)
                            .inErrorMessageId(new MessageId(String.format("%s%s",
                                    programStatusDataStructure.getExceptionType(),
                                    programStatusDataStructure.getExceptionNumber())))
                            .inMessageText(FATAL_ERROR_TEXT)
                            .build();
            terminateBatchActivationGroupService.execute(terminateBatchActivationGroupContext);
        }
    }
}
