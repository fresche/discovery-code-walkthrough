package com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.beginprogram;

import com.vertex.mrdownload.caps.packageleveltypedefinitions.ProcessRequestId;
import com.vertex.mrdownload.caps.processentrymethods.createjobscopeprocessdatatable.CreateJobScopeProcessDataTableContext;
import com.vertex.mrdownload.caps.processentrymethods.loadprocessentrydatastructure.LoadProcessEntryDataStructureContext;
import com.vertex.mrdownload.caps.processrequestmethods.getprocessrequestattributes.GetProcessRequestAttributesContext;
import com.vertex.mrdownload.caps.processtypemethods.getprocesstypeattributes.GetProcessTypeAttributesContext;
import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.UIBackOfficeProcessDriverMeterReadDownloadContext;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.UIBackOfficeProcessDriverMeterReadDownloadService;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.endprogramwitherror.EndProgramWithErrorContext;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.putprogramprocessingexception.PutProgramProcessingExceptionContext;
import org.springframework.stereotype.Service;

import static java.lang.Integer.parseInt;
import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.trim;

/**
 * =======================================================================
 * @subprocedure beginProgram
 * =======================================================================
 * @purpose  Initialize global variables and settings.
 * =======================================================================
 */
@Service
public class BeginProgramService extends UIBackOfficeProcessDriverMeterReadDownloadService {

    private static final String SUBPROCEDURE_NAME = "beginProgram";

    public void execute(UIBackOfficeProcessDriverMeterReadDownloadContext parentContext, BeginProgramContext context) {
        try {
            // ======================================================================
            // Convert the input Process Request Id from character format to integer.
            // ======================================================================
            parentContext.setProcessRequestId(new ProcessRequestId(parseInt(trim(parentContext.getInProcessRequestIdChar()))));

            // ======================================================================
            // Create / clear a temporary table for storage of Process Id and Task Id.
            // ======================================================================
            CreateJobScopeProcessDataTableContext createJobScopeProcessDataTableContext =
                    CreateJobScopeProcessDataTableContext.builder()
                            .build();
            createJobScopeProcessDataTableService.execute(createJobScopeProcessDataTableContext);
            context.setMessageId(createJobScopeProcessDataTableContext.getOutMessageId());

            // Error-Handling:  Irrecoverable Process-Scope Error
            if (!isBlank(context.getMessageId().getValue())) {
                // TODO: Unsupported Instruction -> dump(A)
                PutProgramProcessingExceptionContext putProgramProcessingExceptionContext =
                        PutProgramProcessingExceptionContext.putProgramProcessingExceptionContextBuilder()
                                .inSubprocedureName(format("%s 1", SUBPROCEDURE_NAME))
                                .inCommentText("Error: createJobScopeProcessDataTable")
                                .build();
                putProgramProcessingExceptionService.execute(putProgramProcessingExceptionContext);
                EndProgramWithErrorContext endProgramWithErrorContext = EndProgramWithErrorContext.builder()
                        .inSubprocedureName(format("%s 1", SUBPROCEDURE_NAME))
                        .inMessageId(context.getMessageId())
                        .inMessageText("")
                        .build();
                endProgramWithErrorService.execute(parentContext, endProgramWithErrorContext);
            }

            // ======================================================================
            // Retrieve the Process Request attribute values.
            // ======================================================================
            GetProcessRequestAttributesContext getProcessRequestAttributesContext =
                    GetProcessRequestAttributesContext.builder()
                            .build();
            parentContext.setDsProcessRequest(
                    getProcessRequestAttributesService.execute(getProcessRequestAttributesContext));

            if (!isBlank(context.getMessageId().getValue())) {
                //    Error-Handling:  Irrecoverable Process-Scope Error
                // TODO: Unsupported Instruction -> dump(A)
                PutProgramProcessingExceptionContext putProgramProcessingExceptionContext =
                        PutProgramProcessingExceptionContext.putProgramProcessingExceptionContextBuilder()
                                .inSubprocedureName(format("%s 2", SUBPROCEDURE_NAME))
                                .inCommentText("Error: getProcessRequestAttributes")
                                .build();
                putProgramProcessingExceptionService.execute(putProgramProcessingExceptionContext);
                EndProgramWithErrorContext endProgramWithErrorContext = EndProgramWithErrorContext.builder()
                        .inSubprocedureName(format("%s 2", SUBPROCEDURE_NAME))
                        .inMessageId(context.getMessageId())
                        .inMessageText("")
                        .build();
                endProgramWithErrorService.execute(parentContext, endProgramWithErrorContext);
            }

            parentContext.setBatchId(parentContext.getDsProcessRequest().getPrBatchId());

            // ======================================================================
            // Retrieve the Process Type attribute values.
            // ======================================================================
            GetProcessTypeAttributesContext getProcessTypeAttributesContext =
                    GetProcessTypeAttributesContext.builder()
                            .inProcessType(parentContext.getDsProcessRequest().getPrProcessType())
                            .build();
            parentContext.setDsProcessType(
                    getProcessTypeAttributesService.execute(getProcessTypeAttributesContext));
            context.setMessageId(getProcessRequestAttributesContext.getOutMessageId());

            if (!isBlank(context.getMessageId().getValue())) {
                //    Error-Handling:  Irrecoverable Process-Scope Error
                // TODO: Unsupported Instruction -> dump(A)
                PutProgramProcessingExceptionContext putProgramProcessingExceptionContext =
                        PutProgramProcessingExceptionContext.putProgramProcessingExceptionContextBuilder()
                                .inSubprocedureName(format("%s 3", SUBPROCEDURE_NAME))
                                .inCommentText("Error: getProcessTypeAttributes")
                                .build();
                putProgramProcessingExceptionService.execute(putProgramProcessingExceptionContext);
                EndProgramWithErrorContext endProgramWithErrorContext = EndProgramWithErrorContext.builder()
                        .inSubprocedureName(format("%s 3", SUBPROCEDURE_NAME))
                        .inMessageId(context.getMessageId())
                        .inMessageText("")
                        .build();
                endProgramWithErrorService.execute(parentContext, endProgramWithErrorContext);
            }

            if (parentContext.getDsProcessType().getPctHasDefaultProcessingProgram())
                parentContext.setDownloadProgram(parentContext.getDsProcessType().getPctDefaultProcessingProgram());
            else
                parentContext.setDownloadProgram("MTRMRDWN");

            // ======================================================================
            // Transfer the Process Request values into the corresponding Process
            // Entry variables.
            // ======================================================================
            LoadProcessEntryDataStructureContext loadProcessEntryDataStructureContext =
                    LoadProcessEntryDataStructureContext.builder()
                            .inProcessRequestDs(parentContext.getDsProcessRequest())
                            .build();
            loadProcessEntryDataStructureService.execute(loadProcessEntryDataStructureContext);
            parentContext.setDsProcessEntry(loadProcessEntryDataStructureContext.getOutProcessEntryDs());
            context.setMessageId(loadProcessEntryDataStructureContext.getOutMessageId());

            if (!isBlank(context.getMessageId().getValue())) {
                //    Error-Handling:  Irrecoverable Process-Scope Error
                // TODO: Unsupported Instruction -> dump(A)
                PutProgramProcessingExceptionContext putProgramProcessingExceptionContext =
                        PutProgramProcessingExceptionContext.putProgramProcessingExceptionContextBuilder()
                                .inSubprocedureName(format("%s 4", SUBPROCEDURE_NAME))
                                .inCommentText("Error: loadProcessEntryDataStructure")
                                .build();
                putProgramProcessingExceptionService.execute(putProgramProcessingExceptionContext);
                EndProgramWithErrorContext endProgramWithErrorContext = EndProgramWithErrorContext.builder()
                        .inSubprocedureName(format("%s 4", SUBPROCEDURE_NAME))
                        .inMessageId(context.getMessageId())
                        .inMessageText("")
                        .build();
                endProgramWithErrorService.execute(parentContext, endProgramWithErrorContext);
            }

            return;
        }
        catch (Exception e) {
            // Error-Handling:  Irrecoverable Process-Scope Error
            // TODO: Unsupported Instruction -> dump(A)
            PutProgramProcessingExceptionContext putProgramProcessingExceptionContext =
                    PutProgramProcessingExceptionContext.putProgramProcessingExceptionContextBuilder()
                            .inSubprocedureName(format("%s 0", SUBPROCEDURE_NAME))
                            .inCommentText("Program Processing Error")
                            .build();
            putProgramProcessingExceptionService.execute(putProgramProcessingExceptionContext);
            EndProgramWithErrorContext endProgramWithErrorContext = EndProgramWithErrorContext.builder()
                    .inSubprocedureName(SUBPROCEDURE_NAME)
                    .inMessageId(new MessageId(format("%s%s",
                            programStatusDataStructure.getExceptionType(),
                            programStatusDataStructure.getExceptionNumber())))
                    .inMessageText(programStatusDataStructure.getRetrievedExceptionData())
                    .build();
            endProgramWithErrorService.execute(parentContext, endProgramWithErrorContext);
        }
    }
}
