package com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.endprogram;

import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class EndProgramContext {

    // Local Variables
    private MessageId messageId;
}
