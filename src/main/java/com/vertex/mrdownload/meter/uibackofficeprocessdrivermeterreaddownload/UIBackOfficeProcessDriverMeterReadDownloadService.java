package com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload;

import com.vertex.mrdownload.BaseService;
import com.vertex.mrdownload.ProgramStatusDataStructure;
import com.vertex.mrdownload.caps.processentrymethods.addprocessentry.AddProcessEntryService;
import com.vertex.mrdownload.caps.processentrymethods.completeprocessentry.CompleteProcessEntryService;
import com.vertex.mrdownload.caps.processentrymethods.createjobscopeprocessdatatable.CreateJobScopeProcessDataTableService;
import com.vertex.mrdownload.caps.processentrymethods.loadprocessentrydatastructure.LoadProcessEntryDataStructureService;
import com.vertex.mrdownload.caps.processentrymethods.putjobscopeprocessdata.PutJobScopeProcessDataService;
import com.vertex.mrdownload.caps.processrequestmethods.getprocessrequestattributes.GetProcessRequestAttributesService;
import com.vertex.mrdownload.caps.processtypemethods.getprocesstypeattributes.GetProcessTypeAttributesService;
import com.vertex.mrdownload.global.commitmentcontrolmethods.commitchanges.CommitChangesService;
import com.vertex.mrdownload.global.enterpriseleveltypedefinitions.MessageId;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.beginprogram.BeginProgramContext;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.beginprogram.BeginProgramService;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.endprogram.EndProgramContext;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.endprogram.EndProgramService;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.endprogramwitherror.EndProgramWithErrorContext;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.endprogramwitherror.EndProgramWithErrorService;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.executeprocess.ExecuteProcessService;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.processrequest.ProcessRequestContext;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.processrequest.ProcessRequestService;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.putprogramprocessingexception.PutProgramProcessingExceptionContext;
import com.vertex.mrdownload.meter.uibackofficeprocessdrivermeterreaddownload.putprogramprocessingexception.PutProgramProcessingExceptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import static java.lang.String.format;

/**
 * =======================================================================
 * System Id    -  eCIS+:  Meter-Reading Package
 * Member Name  -  MTRMRDDRV2
 * Member Title -  UI Back-Office Process Driver - Meter Read Download
 * Written By   -  Laura Meier (LAURAM)
 * Date Written -  08/05/2010
 * =======================================================================
 * Purpose / Description:
 *     This is the driver program for executing Meter Read Download from
 *     a UI Back-Office process request.
 * =======================================================================
 * Parameters:
 *     Refer to the Prototype Definition for parameter documentation.
 * =======================================================================
 * Compilation / Binding Instructions:
 *     1)  Use MKS Object Type RPGSILE.
 * =======================================================================
 * Modifications:
 *
 * Date        Jira Task Id     DT#    DR#    Version     Developer
 * ----------  ---------------  -----  -----  ----------  ----------------
 * 08/05/2010  Jira ECIS-1245   55587  55595  eCIS+ V3R2  Laura Meier
 * Initial Version
 *
 * 01/07/2011  n/a              56086  56109  eCIS+ V3R2  Jeff Haddix
 * Modify in support of putException parameter change.
 *
 * 03/03/2011  Jira ECIS-2197   56315  56315  eCIS+ V3R2  Jeff Haddix
 * Modified the process program (MTRMRDWN) interface, removing parameter
 * outMessageId.
 *
 * 09/20/2013  Jira ECIS-18173  60324  60324  eCIS+ V3R2  Jeff Haddix
 * In order to reduce the incidence of the issues that we are experiencing
 * with UPRCRQS locking issues, this driver program is being modified to
 * call procedure deleteProcessRequest_APSPRCRQS at an earlier point in
 * time.  Previously the UPRCRQS entry was being deleted at the end of the
 * driver program.  This delete has been moved to immediately prior to
 * execution of the processing program.
 *
 * 03/14/2016  Jira LCIMP-41    63851  63951  eCIS+ V3R4  Sharla Polley
 *  Remove hard-coded MTRMRDWN to use the Default Processing Program from
 *  the Process Type instead (default to MTRMRDWN)
 *  Remove the messageId from Commit/Rollback
 *
 * 02/07/2018  Jira ECIS-25701  66773  66773  V3R5 P01    Jeff Haddix
 * 02/07/2018  Jira ECIS-25701  66773  66774  V3R6        Jeff Haddix
 * 1) Remove obsolete message id parameter from calls to commitChanges.
 * =======================================================================
 */
@Service
public class UIBackOfficeProcessDriverMeterReadDownloadService extends BaseService {

    protected static final String SUBPROCEDURE_NAME = "MTRMRDDRV2 Mainline";

    @Autowired
    protected ApplicationContext appCtx;

    @Autowired
    protected ProgramStatusDataStructure programStatusDataStructure;

    @Autowired
    protected AddProcessEntryService addProcessEntryService;

    @Autowired
    protected CommitChangesService commitChangesService;

    @Autowired
    protected PutJobScopeProcessDataService putJobScopeProcessDataService;

    @Autowired
    protected ExecuteProcessService executeProcessService;

    @Autowired
    protected CompleteProcessEntryService completeProcessEntryService;

    @Autowired
    protected CreateJobScopeProcessDataTableService createJobScopeProcessDataTableService;

    @Autowired
    protected GetProcessRequestAttributesService getProcessRequestAttributesService;

    @Autowired
    protected GetProcessTypeAttributesService getProcessTypeAttributesService;

    @Autowired
    protected LoadProcessEntryDataStructureService loadProcessEntryDataStructureService;

    @Autowired
    protected BeginProgramService beginProgramService;

    @Autowired
    protected ProcessRequestService processRequestService;

    @Autowired
    protected EndProgramService endProgramService;

    @Autowired
    protected PutProgramProcessingExceptionService putProgramProcessingExceptionService;

    @Autowired
    protected EndProgramWithErrorService endProgramWithErrorService;

    public UIBackOfficeProcessDriverMeterReadDownloadService() {
        super("MTRMRDDRV2");
    }

    public void execute(UIBackOfficeProcessDriverMeterReadDownloadContext context) {
        // ======================================================================
        //                            Mainline Program
        // ======================================================================
        try {
            BeginProgramContext beginProgramContext = BeginProgramContext.builder()
                    .build();
            beginProgramService.execute(context, beginProgramContext);

            ProcessRequestContext processRequestContext = ProcessRequestContext.builder()
                    .build();
            processRequestService.execute(context, processRequestContext);

            EndProgramContext endProgramContext = EndProgramContext.builder()
                    .build();
            endProgramService.execute(context, endProgramContext);

            return;
        }
        catch (Exception e) {
            // Error-Handling:  Irrecoverable Process-Scope Error
            // TODO: Unsupported Instruction -> dump(A)

            PutProgramProcessingExceptionContext putProgramProcessingExceptionContext =
                    PutProgramProcessingExceptionContext.putProgramProcessingExceptionContextBuilder()
                            .inSubprocedureName(format("%s 0", SUBPROCEDURE_NAME))
                            .inCommentText("Program Processing Error")
                            .build();
            putProgramProcessingExceptionService.execute(putProgramProcessingExceptionContext);
            EndProgramWithErrorContext endProgramWithErrorContext = EndProgramWithErrorContext.builder()
                    .inSubprocedureName(SUBPROCEDURE_NAME)
                    .inMessageId(new MessageId(format("%s%s",
                            programStatusDataStructure.getExceptionType(),
                            programStatusDataStructure.getExceptionNumber())))
                    .inMessageText(programStatusDataStructure.getRetrievedExceptionData())
                    .build();
            endProgramWithErrorService.execute(context, endProgramWithErrorContext);
            return;
        }

    }
}
