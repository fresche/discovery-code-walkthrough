package com.vertex.mrdownload;

import lombok.Data;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Data
@Component
@Scope("application")
public class ProgramStatusDataStructure {

    private String programName;
    private String exceptionType;
    private String exceptionNumber;
    private String jobName;
    private String jobUser;
    private int jobNumber;
    private String retrievedExceptionData;
}
