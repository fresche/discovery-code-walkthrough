package com.vertex.mrdownload;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MrdownloadApplication {

    public static void main(String[] args) {
        SpringApplication.run(MrdownloadApplication.class, args);
    }

}
