package com.vertex.mrdownload.serviceorder.packageleveltypedefinitions;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class SequenceNumber {

    private int value;
}
